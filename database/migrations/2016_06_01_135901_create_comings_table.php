<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comings', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->time('intime');
            $table->time('outtime');
            $table->dateTime('day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
