<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->datetime('deadline');
            $table->boolean('fire');
            $table->integer('user_id');
            $table->integer('author_id');
            $table->boolean('favorite');
            $table->string('status');
            $table->integer('percent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
