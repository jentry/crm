/**
 * Created by jenya on 10.05.2016.
 */
function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}
$(document).ready(function(){
    var $_token = $('meta[name="csrf-token"]').attr('content');
    var _form = $('#searchForm');
    _form.on('submit', function(e){
        e.preventDefault();
//        var _site = $('input[name="site"]', this).val();
        var _zapros = $('input[name="zapros"]',this).val();
        var _url = $(this).attr('action');

        $.ajax({
            url: _url,
            type: "POST",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {zapros : _zapros},
            beforeSend: function(){
                $('button',_form).attr('disabled', true);
                $('.clients .loading').fadeIn(200);
            },
            success: function(data){
				console.log(data);
                $('.clients .loading').fadeOut(200);
                $('p.results span.zap').text(_zapros);
                $('p.results span.pos').text(data['rand']);
                $('button',_form).attr('disabled', false);
                $.views.settings.delimiters("{%","%}");
                var tmpl = $.templates("#myTmpl2");
                $('#searchForm input[name="site"]').val(data[0][0][0]);
                var _renderData = {
                    site1 : data[0][0][0],
                    site2 : data[0][1][0],
                    site3 : data[0][2][0],
                    site4 : data[0][3][0],
                    site1P1 : data[1][0][0][0],
                    site1P2 : data[1][0][0][1],
                    site1P3 : data[1][0][0][2],
                    site1P4 : data[1][0][1][0],
                    site1P5 : data[1][0][1][1],
                    site1P6 : data[1][0][1][2],
                    site2P1 : data[1][1][0][0],
                    site2P2 : data[1][1][0][1],
                    site2P3 : data[1][1][0][2],
                    site2P4 : data[1][1][1][0],
                    site2P5 : data[1][1][1][1],
                    site2P6 : data[1][1][1][2],
                    site3P1 : data[1][2][0][0],
                    site3P2 : data[1][2][0][1],
                    site3P3 : data[1][2][0][2],
                    site3P4 : data[1][2][1][0],
                    site3P5 : data[1][2][1][1],
                    site3P6 : data[1][2][1][2],
                    site4P1 : data[1][3][0][0],
                    site4P2 : data[1][3][0][1],
                    site4P3 : data[1][3][0][2],
                    site4P4 : data[1][3][1][0],
                    site4P5 : data[1][3][1][1],
                    site4P6 : data[1][3][1][2]
                };
                var html = tmpl.render(_renderData);
                $("#infoContent").html(html);

                $('.clients .new_clients .form p.results span.zap').text(data.zapros);
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });


    //info for random site
    $('.clients .site_info form').on('submit', function(e){
        e.preventDefault();
        var self = $(this);
        var _site = $('input[name="site_url"]',this).val();

        $.ajax({
            url : '/client/randomsite',
            type: "POST",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {site : _site},
            beforeSend: function(){
                $('#infoContent').addClass('load');
            },
            success: function(data){
                $('#infoContent').removeClass('load');
                $.views.settings.delimiters("{%","%}");
                var tmpl = $.templates("#myTmpl");
                var _renderData = {
                    randomSite : _site,
                    site1P1 : data[0][0][0],
                    site1P2 : data[0][0][1],
                    site1P3 : data[0][0][2],
                    site1P4 : data[0][1][0],
                    site1P5 : data[0][1][1],
                    site1P6 : data[0][1][2]
                };
                var html = tmpl.render(_renderData);
                $("#infoContent").html(html);
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //add site to like comments
    $('#likeButton').on('click', function(e){
        e.preventDefault();
        var _formBlock = $(this).closest('.form');
        var _siteAddress = $('input[name="site"]',_formBlock).val();
        $('#myModal').modal('show');
        $('#addYesForm input[name="site"]').val(_siteAddress);
    });
    $('#addYesForm').on('submit', function(e){
        e.preventDefault();
        var self = $(this);
        var _site = $('input[name="site"]',self).val();
        var _time = $('input[name="deadline"]',self).val();
        var _type = $('select option:selected',self).text();
        var _comment = $('textarea[name="comment"]',self).val();


        $.ajax({
            type: "POST",
            url: "/client/add",
            headers: { 'X-CSRF-TOKEN' : $_token },
            dataType: 'json',
            data: {site : _site, time : _time, type : _type, comment : _comment, isLike : 1, blacklist :0},
            success: function(data){
                location.reload();
                $('button',_form).attr('disabled', false);
            },
            error: function(data){
                console.log(data);
            }
        });
    });


    //add site to dislike comments
    $('#dislikeButton').on('click', function(e){
        e.preventDefault();
        var _formBlock = $(this).closest('.form');
        var _siteAddress = $('input[name="site"]',_formBlock).val();
        $('#myModal2').modal('show');
        $('#addNoForm input[name="site"]').val(_siteAddress);
    });
    $('#addNoForm').on('submit', function(e){
        e.preventDefault();
        var self = $(this);
        var _site = $('input[name="site"]',self).val();
        var _comment = $('textarea[name="comment"]',self).val();


        $.ajax({
            type: "POST",
            url: "/client/add",
            headers: { 'X-CSRF-TOKEN' : $_token },
            dataType: 'json',
            data: {site : _site,  time : '', type : '', comment : _comment, isLike : 0, blacklist :0},
            success: function(data){
                location.reload();
                $('button',_form).attr('disabled', false);
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //add site to blacklist
    $('#toBlacklist').on('click', function(e){
        e.preventDefault();
        var _formBlock = $(this).closest('.form');
        var _siteAddress = $('input[name="site"]',_formBlock).val();
        $('#myModal3').modal('show');
        $('#addBlackForm input[name="site"]').val(_siteAddress);
    });
    $('#addBlackForm').on('submit', function(e){
        e.preventDefault();
        var self = $(this);
        var _site = $('input[name="site"]',self).val();
        var _comment = $('textarea[name="comment"]',self).val();


        $.ajax({
            type: "POST",
            url: "/client/add",
            headers: { 'X-CSRF-TOKEN' : $_token },
            dataType: 'json',
            data: {site : _site,  time : '', type : '', comment : _comment, isLike : 0, blacklist :1},
            success: function(data){
                location.reload();
                $('button',_form).attr('disabled', false);
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //toggle type sites in list
    $('.comments_block .toggle a').on('click', function(e){
        e.preventDefault();
        var _type = $(this).attr('data-type');
        var _isLike = 1;
        var _blacklist = 0;
        $('.comments_block .toggle a').removeClass('active');
        $(this).addClass('active');
        if(_type == 'like'){
            _isLike = 1;
            _blacklist = 0;
        }
        else if(_type == 'dislike'){
            _isLike = 0;
            _blacklist = 0;
        }
        else if(_type == 'blacklist'){
            _isLike = null;
            _blacklist = 1;
        }
        else if(_type == 'project'){
            _isLike = null;
            _blacklist = 'project';
        }

        $.ajax({
            type: "POST",
            url : "/client/sort",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {isLike : _isLike, blacklist : _blacklist},
            success: function(data){
                /*$.views.settings.delimiters("{%","%}");
                var tmpl = $.templates("#listTmpl");
                var htmlOutput = tmpl.render(data);*/
                $('#clientsTable').html(data);
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //edit client
    $(document).on('click', '#clientsTable a.full', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');
        var _type = $(this).attr('data-type');
        $.ajax({
            type: "POST",
            url : "/client/edit",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#myModal4 .modal-body').html(data);
                if(_type == 'deadline'){
                    $('#editForm input[name="site"], #editForm select, #editForm textarea').hide();
                }
                else if(_type == 'status'){
                    $('#editForm input[name="site"], #editForm input[name="deadline"], #editForm textarea').hide();
                }
                $('#myModal4').modal('show');
                $('.datetimepicker').datetimepicker({
                    format : "D MM YYYY",
                    locale: "ru"
                });

            }
        });
    });

    //edit client's deadline
    $(document).on('click', '#clientsTable a.deadline', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');
        var self = $(this);
        var _block = $('.fordatetimepicker');
        var _left = self.offset().left;
        var _top = self.offset().top - 60;
        $.ajax({
            type: "POST",
            url : "/client/edit/deadline",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('input',_block).datetimepicker({
                    format : "D MM YYYY",
                    locale: "ru",
                    inline: true
                }).on('dp.change', function(e){
                    var _time = e.target.value;

                    $.ajax({
                        type: "PUT",
                        url: "/client/update/deadline",
                        headers: { 'X-CSRF-TOKEN' : $_token },
                        data: {time : _time, id : _id},
                        success: function(data){
                            self.text(_time);
                            _block.hide();
                        }
                    });
                });
                _block.css({'top' : _top, 'left' : _left}).show();

            },
            error: function(data){
                console.log(data);
            }
        });
    });

    $(document).on('click', function(event){
            var div = $(".fordatetimepicker");
            if($(event.target).closest(".fordatetimepicker").length === 0 && ($(event.target).closest("a.deadline").length === 0)){
                div.fadeOut(200);
            }
    });


    //edit client's status
    var statusLink;
    $(document).on('click', '#clientsTable a.status', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');
        statusLink = $(this);
        var _block = $('.forchangestatus');
        var _left = statusLink.offset().left;
        var _top = statusLink.offset().top - 60;
        $.ajax({
            type: "POST",
            url : "/client/edit/status",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                _block.html(data).css({'top' : _top, 'left' : _left}).show();
            },
            error: function(data){
                console.log(data);
            }
        });
    });
    $(document).on('change', '.forchangestatus select', function(){
        var self = $(this);
        var _status = $('.forchangestatus select option:selected').text();
        var _id = $('.forchangestatus input[name="id"]').val();
        $.ajax({
            type: "PUT",
            url : "/client/update/status",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {status : _status, id : _id},
            success: function(data){
                statusLink.text(_status);
                $('.forchangestatus').hide();
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });
    $(document).on('click', function(event){
        var div = $(".forchangestatus");
        if($(event.target).closest(".forchangestatus").length === 0 && ($(event.target).closest("a.status").length === 0)){
            div.fadeOut(200);
        }
    });


    $(document).on('submit', '#editForm', function(e){
        e.preventDefault();
        var self = $(this);
        var _id = $('input[name="id"]',self).val();
        var _site = $('input[name="site"]',self).val();
        var _time = $('input[name="deadline"]',self).val();
        var _type = $('select option:selected',self).text();
        var _comment = $('textarea[name="comment"]',self).val();


        $.ajax({
            type: "PUT",
            url: "/client/edit",
            headers: { 'X-CSRF-TOKEN' : $_token },
            dataType: 'json',
            data: {id : _id, site : _site, time : _time, type : _type, comment : _comment},
            success: function(data){
                location.reload();
                //$('#myModal4').modal('hide');
            },
            error: function(data){
                console.log(data);
            }
        });
    });


    //show comments to client
    $(document).on('click', '#clientsTable a.comments', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url : "/client/comments",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#commentsModal .modal-body').html(data);
                $('#commentsModal').modal('show');
                $('#addCommentForm input[name="id"]').val(_id);
            }
        });
    });

    //add comments to clients
    $(document).on('submit', '#addCommentForm', function(e){
        e.preventDefault();
        var self = $(this);
        var _client = $('input[name="id"]',self).val();
        var _text = $('textarea',self).val();
        if(_text != '') {
            $.ajax({
                type: "POST",
                url: "/client/comments/add",
                headers: {'X-CSRF-TOKEN': $_token},
                data: {client: _client, text: _text},
                success: function (data) {
                    $('#commentsModal .modal-body').html(data);
                    $('textarea', self).val('');
                }
            });
        }
        else{
            alert('Вы не заполнили поле комментарий');
        }
    });

    //show users for create task
    $('.slideDown').on('click', function(e){
        e.preventDefault();
        var _block = $('#'+$(this).attr('data-block'));
        var self = $(this);
        if(self.hasClass('open')){
            _block.slideUp(200);
            self.removeClass('open');
        }
        else{
            _block.slideDown(200);
            self.addClass('open');
        }
    });

    //add or remove fav from task
    $(document).on('click', 'a.fav', function(e){
        e.preventDefault();
        var _type = $(this).attr('data-type');
        var _id = $(this).attr('data-id');
        var self = $(this);
        if(_type == 'add'){
            $.ajax({
                type: "PUT",
                url: "/task/changefav",
                headers: { 'X-CSRF-TOKEN' : $_token },
                data: {id : _id, type : 1},
                success: function(data){
                    self.removeClass('empty').addClass('full').attr('data-type', 'remove').html('<i class="fa fa-star" aria-hidden="true"></i>');
                },
                error: function(data){

                }
            });
        }
        else{
            $.ajax({
                type: "PUT",
                url: "/task/changefav",
                headers: { 'X-CSRF-TOKEN' : $_token },
                data: {id : _id, type : 0},
                success: function(data){
                    self.removeClass('full').addClass('empty').attr('data-type', 'add').html('<i class="fa fa-star-o" aria-hidden="true"></i>');
                },
                error: function(data){

                }
            });
        }
    });

    //show sub-tasks
    $(document).on('click', '#tasks_table a.clicker', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');
        var self = $(this);
        var _tr = self.closest('tr');
        if(self.hasClass('opened')){
            $('#tasks_table tr.child'+_id).remove();
            self.removeClass('opened').html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>');
        }
        else{
            $.ajax({
                type: "POST",
                url: "/task/showsub",
                headers: { 'X-CSRF-TOKEN' : $_token },
                data: {id : _id},
                success: function(data){
                    _tr.after(data);
                    self.addClass('opened').html('<i class="fa fa-minus-square-o" aria-hidden="true"></i>');
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
    });

    //edit task comment
    $('a.edit_comment').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            url: "/taskcomment/get",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#commentsModal .modal-body').html(data);
                $('#commentsModal').modal('show');
            },
            error: function(data){

            }
        });
    });

    //change susers in task
    $('#changeSusers').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            url: "/susers/get",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#modal1 .modal-body').html(data);
                $('#modal1').modal('show');
            },
            error: function(data){

            }
        });
    });

    //change auditors in task
    $('#changeAuditors').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            url: "/auditors/get",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#modal2 .modal-body').html(data);
                $('#modal2').modal('show');
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //change percent in task
    $('a.changePercent').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            url: "/task/percent",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#changePercent input').val(data.percent);
                $('#changePercent').removeClass('hide');
            },
            error: function(data){
                console.log(data);
            }
        })
    });
    $('#changeButton1').on('click', function(){
       var _percents = $('input[name="percents"]').val();
       var _id = $(this).attr('data-id');

        $.ajax({
            type: "PUT",
            url: "/task/percent",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id, percents : _percents},
            success: function(data){
                $('#changePercent').addClass('hide');
                $('p.percent span').text(_percents);
            },
            error: function(data){
                console.log(data);
            }
        });
    });


    //change status in task
    $('a.changeStatus').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            url: "/task/status",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(data){
                $('#changeStatus select option').each(function(){
                   if($(this).val() == data.status){
                       $(this).attr('selected', true);
                   }
                });
                $('#changeStatus').removeClass('hide');
            },
            error: function(data){
                console.log(data);
            }
        })
    });
    $('#changeButton2').on('click', function(){
        var _status = $('#changeStatus select option:selected').text();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "PUT",
            url: "/task/status",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id, status : _status},
            success: function(data){
                $('#changeStatus').addClass('hide');
                $('p.status span').text(_status);
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //finish task
    $('#changeFinishedButton').on('click', function(){
       var _id = $(this).attr('data-id');
        var _state = $(this).attr('data-state');

        $.ajax({
            type: "POST",
            url: "/task/finish",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id, state: _state},
            success: function(){
                location.reload();
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //delete message
    $('a.delete_mess').on('click',function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id');

        $.ajax({
            type: "DELETE",
            url: "/message/delete",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {id : _id},
            success: function(){
                location.reload();
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    //sort tasks by types
    $('a.sort_tasks_link').on('click', function(e){
        e.preventDefault();
        var _type = $(this).attr('data-type');
        var _text = $(this).text();
        var _parent = $(this).parent('li');
        $.ajax({
            type: "POST",
            url: "/tasks/sort/params",
            headers: { 'X-CSRF-TOKEN' : $_token },
            data: {type : _type},
            success: function(data){
                //console.log(data);
                $('h1').text(_text);
                $('#tasks_table tbody').html(data);
                $('aside ul li').removeClass('active');
                _parent.addClass('active');
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });

    //edit premia's summ
    $('a.edit-premia').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id'),
            _summ = $(this).attr('data-summ');
        var _form = $('#myModal6 form');
        $('input[name="id"]', _form).val(_id);
        $('input[name="summ"]', _form).val(_summ);
        $('#myModal6').modal('show');
    });

    //edit avans's summ
    $('a.edit-avans').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id'),
            _summ = $(this).attr('data-summ');
        var _form = $('#myModal6 form');
        $('input[name="id"]', _form).val(_id);
        $('input[name="summ"]', _form).val(_summ);
        $('#myModal6').modal('show');
    });

    $('a.edit-daysalary').on('click', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id'),
            _summ = $(this).attr('data-summ');
        var _form = $('#myModal6 form');
        $('input[name="id"]', _form).val(_id);
        $('input[name="summ"]', _form).val(_summ);
        $('#myModal6').modal('show');
    });

    $('footer .tasks a.labels').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        var _list = self.next('.list');
        if(self.hasClass('open')){
            _list.fadeOut(200);
            self.removeClass('open');
        }
        else{
            _list.fadeIn(200);
            self.addClass('open');
        }
    });

    $('footer .nots a.labels').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        var _list = self.next('.list');
        if(self.hasClass('open')){
            _list.fadeOut(200);
            self.removeClass('open');
        }
        else{
            _list.fadeIn(200);
            self.addClass('open');
            $.ajax({
                type: "POST",
                url: "/notifications/reads",
                headers: { 'X-CSRF-TOKEN' : $_token },
                data: {type: 1},
                success: function(data){

                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }
    });
    $('footer .attans a.labels').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        var _list = self.next('.list');
        if(self.hasClass('open')){
            _list.fadeOut(200);
            self.removeClass('open');
        }
        else{
            _list.fadeIn(200);
            self.addClass('open');
            $.ajax({
                type: "POST",
                url: "/notifications/reads",
                headers: { 'X-CSRF-TOKEN' : $_token },
                data: {type: 2},
                success: function(data){

                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }
    });
});