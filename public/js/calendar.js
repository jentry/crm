
    //render calendar on page
    function getDay(date){
        var day = date.getDay();
        if(day == 0){
            day = 7;
        }
        return day - 1;
    }


   function renderCalendar(year, month, userId){
       var d = new Date(year, month-1),
           _jsMonth = month - 1,
           nowD = new Date(),
           now = nowD.getDate(),
           _html = '<tr>';


       for(var i = 0; i < getDay(d); i++){
           _html += '<td></td>';
       }


       while (d.getMonth() == _jsMonth) {
           if(getDay(d) == 5 || getDay(d) == 6){
               if(d.getDate() == now){
                   _html += '<td class="holiday today"><div><span class="number"> ' + d.getDate() + '</span></div></td>';
               }
               else{
                   _html += '<td class="holiday"><div><span class="number"> ' + d.getDate() + '</span></div></td>';
               }

           }
           else{

               var _x = d.getDate();
               if(d.getDate() == now){
                   _html += '<td class="today"><div id="day'+d.getDate()+'"><span class="number"> ' + d.getDate()  + '</span><p class="time">'+ _data[_x].startJob +' - '+ _data[_x].finishJob +'</p><p class="daySalary">Раб. день - <span> '+ _data[_x].resSumm +' руб.</span></p><p class="specialSalary">Договор - <span>' + _data[_x].premia.summ + ' руб.</span></p></div></td>';
               }
               else{
                   _html += '<td><div id="day'+d.getDate()+'"><span class="number"> ' + d.getDate()  + '</span><p class="time">'+ _data[_x].startJob +' - '+ _data[_x].finishJob +'</p><p class="daySalary">Раб. день - <span> '+ _data[_x].resSumm +' руб.</span></p><p class="specialSalary">Договор - <span>' + _data[_x].premia.summ + ' руб.</span></p></div></td>';
               }

           }


           if (getDay(d) % 7 == 6) {
               _html += '</tr><tr>';
           }

           d.setDate(d.getDate() + 1);
       }


       if (getDay(d) != 0) {
           for (var i = getDay(d); i < 7; i++) {
               _html += '<td></td>';
           }
       }

       _html += '</tr>';


        $('#calendar').html(_html);

   }



