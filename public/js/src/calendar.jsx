import React from 'react';
import ReactDOM from 'react-dom';
import Head from './head';
import Show from './show';

class Day extends React.Component{
    getInitialState(){
        this.setState = true;
    }

    render(){
        return(
          <td>
            <div>
                <span className="number">{this.props.data.number}</span>
                <p className="time">9.00 - 18.00</p>
                <p className="daySalary">Раб. день - <span>900 руб.</span></p>
                <p className="specialSalary">Договор - <span>1 500 руб.</span></p>
            </div>
          </td>
        );
    }
}

class Calendar extends React.Component{

    constructor(year, month){
        super();
        this.props.d = new Date(year, month);
    }

    componentDidMount(){

    }

    render(){
        return(
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        );
    }
}


class App extends React.Component{
    render(){
        return(
            <table>
                <Head />
                <Calendar/>
            </table>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('calendarApp')
);