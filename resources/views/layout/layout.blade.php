<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/screen.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/jquery.bxslider.css')}}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
    <link href="{{asset('assets/vendor/jquery-form-styler/jquery.formstyler.css')}}" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CRM</title>
</head>
<body>
<header>
    <a href="{{ url('/') }}" id="logo"></a>
	@if (!Auth::guest())
    <nav>
        <ul class="list-unstyled group">
            <li>
                <a href="{{url('/newclient')}}">
                    <img src="{{asset('images/icon1.png')}}" alt="" class="img-responsive center-block">
                   Новый клиент
                </a>
            </li>
            <li>
                <a href="{{url('/tasks')}}">
                    <img src="{{asset('images/1.png')}}" alt="" class="img-responsive center-block">
                    Задачи
                </a>
            </li>
            <li>
                <a href="{{url('/salary')}}">
                    <img src="{{asset('images/icon3.png')}}" alt="" class="img-responsive center-block">
                    Зарплата
                </a>
            </li>
            <li class="active">
                <a href="{{url('/managers')}}">
                    <img src="{{asset('images/icon4.png')}}" alt="" class="img-responsive center-block">
                    Сотрудники
                </a>
            </li>
            <li>
                <a href="{{url('/messages')}}">
                    <img src="{{asset('images/icon5.png')}}" alt="" class="img-responsive center-block">
                    Общение
                </a>
            </li>
            @if(Auth::user()->is('admin'))
            <li style="width:40px;">
                <a href="{{url('/faq')}}">
                    <img src="{{asset('images/icon5.png')}}" alt="" class="img-responsive center-block">
                    F.A.Q.
                </a>
            </li>
                <li style="width:40px;">
                    <a href="{{url('/premia')}}">
                        <img src="{{asset('images/icon3.png')}}" alt="" class="img-responsive center-block">
                        Премии
                    </a>
                </li>
                <li style="width:40px; " class="hide">
                    <a href="{{url('/avans')}}">
                        <img src="{{asset('images/icon3.png')}}" alt="" class="img-responsive center-block">
                        Аванс
                    </a>
                </li>
                <li style="width:60px;">
                    <a href="{{url('/daysalary')}}">
                        <img src="{{asset('images/icon3.png')}}" alt="" class="img-responsive center-block">
                        Оплата за день
                    </a>
                </li>
                <li style="width:40px;">
                    <a href="{{url('/managersrole')}}">
                        <img src="{{asset('images/icon4.png')}}" alt="" class="img-responsive center-block">
                        Роли
                    </a>
                </li>
            @endif
        </ul>
    </nav>
	@endif
    @if (Auth::guest())
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/login') }}">Войти</a></li>
            </ul>
        </div>
    @else
    <div class="user_card group">
        <div class="foto">
            <img src="{{asset('/upload/users/'.Auth::user()->foto)}}" alt="" class="img-responsive">
        </div>
        <div class="info">
            <p class="name">{{ Auth::user()->name }}</p>
            <p class="role">{{ Auth::user()->otdel }}</p>
            <a href="{{url('/helping')}}" class="help">помощь</a>
            <a href="{{url('/manager/profile')}}" class="settings">настройки</a>
            <a href="{{url('/logout')}}">Выйти</a>
        </div>
    </div>
    @endif
</header>
@yield('content')

@include('layout.footer')
<script type="text/javascript" src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jsrender.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.bxslider.min.js')}}"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="{{asset('assets/vendor/moment/min/moment-with-locales.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendor/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendor/jquery-form-styler/jquery.formstyler.js')}}"></script>
@include('tmpl.jstmpl')

<script type="text/javascript">
    function showHideUsers(self){
        var _block = $('#users1');
        if(_block.css('display') == 'none'){
            _block.slideDown();
        }
        else{
            _block.slideUp();
        }
    }
    $(document).ready(function(){
        var slider = $('.bxslider').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            pager: false,
            controls: true,
            infiniteLoop: false
        });
        $('.textarea').ckeditor();
        //play audio from faq
        $('#playAudio').on('click', function(e){
            e.preventDefault();
            var _current = slider.getCurrentSlide();
            var _li = $('.bxslider li').eq(_current);
            var _audio = $('audio',_li)[0];
            if(!_li.hasClass('played')){
                _audio.play();
                _li.addClass('played');
            }
            else{
                _audio.pause();
                _li.removeClass('played');
            }


        });
        $('#datetimepicker1').datetimepicker({
            format : "D MM YYYY",
            locale: 'ru'
        });
        $('#datetimepicker2').datetimepicker({
            format : "MM YYYY",
            locale: 'ru',
            viewMode : 'months'
        });

        $(document).on('click', '#types1 a', function(e){
        //$('#types1 a').click(function(e){
            e.preventDefault();
            var _role = $(this).attr('data-id');
            if(_role == 'null'){
                $('#list1 ul li').show();
            }
            else{
                $('#list1 ul li').hide();
                $('#list1 ul li[data-type="'+_role+'"]').show();
            }
            $('#types1 a').removeClass('active');
            $(this).addClass('active');
        });

        $('#types2 a').click(function(e){
            e.preventDefault();
            var _role = $(this).attr('data-id');
            if(_role == 'null'){
                $('#list2 ul li').show();
            }
            else{
                $('#list2 ul li').hide();
                $('#list2 ul li[data-type="'+_role+'"]').show();
            }
            $('#types2 a').removeClass('active');
            $(this).addClass('active');
        });

        $('#types3 a').click(function(e){
            e.preventDefault();
            var _role = $(this).attr('data-id');
            if(_role == 'null'){
                $('#list3 ul li').show();
            }
            else{
                $('#list3 ul li').hide();
                $('#list3 ul li[data-type="'+_role+'"]').show();
            }
            $('#types3 a').removeClass('active');
            $(this).addClass('active');
        });

        $('.whom .choose_user').on('click', function(e){
            e.preventDefault();
            var self = $(this);
            if(self.hasClass('open')){
                $('.chooser').slideUp(200);
                self.removeClass('open');
            }
            else{
                $('.chooser').slideDown(200);
                self.addClass('open');
            }
        });

    });
</script>
</body>
</html>