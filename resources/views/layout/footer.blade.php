<?php
use App\Task;
use App\Notification;

function favsTasksCount(){
    $tasks = Task::where([
            ['user_id', Auth::user()->id],
            ['favorite', 1]
    ])->get();

    return $tasks->count();
}
function favsTasks(){
    $tasks = Task::where([
            ['user_id', Auth::user()->id],
            ['favorite', 1]
    ])->get();

    foreach($tasks as $task){
        echo '<li><a href="/task/'.$task->id.'">'.$task->title.'</a></li>';
    }
}

function fireTasksCount(){
    $tasks = Task::where([
            ['user_id', Auth::user()->id],
            ['fire', 1]
    ])->get();

    return $tasks->count();
}
function firesTasks(){
    $tasks = Task::where([
            ['user_id', Auth::user()->id],
            ['fire', 1]
    ])->get();

    foreach($tasks as $task){
        echo '<li><a href="/task/'.$task->id.'">'.$task->title.'</a></li>';
    }
}

function notifFirtsCount(){
    $notifs = Notification::where([
        ['user_id', Auth::user()->id],
        ['type', 1],
        ['readed', false]
    ])->get();
    return $notifs->count();
}
function notifFirtsList(){
    $notifs = Notification::where([
            ['user_id', Auth::user()->id],
            ['type', 1],
            ['readed', false]
    ])->get();
    foreach($notifs as $notif){
        echo '<li><time>'.date('d.m.Y', strtotime($notif->created_at)).'</time><p>'.$notif->message.'</p></li>';
    }
}

function notifSecondCount(){
    $notifs = Notification::where([
            ['user_id', Auth::user()->id],
            ['type', 2],
            ['readed', false]
    ])->get();
    return $notifs->count();
}
function notifSecondList(){
    $notifs = Notification::where([
            ['user_id', Auth::user()->id],
            ['type', 2],
            ['readed', false]
    ])->get();
    foreach($notifs as $notif){
        echo '<li><time>'.date('d.m.Y', strtotime($notif->created_at)).'</time><p>'.$notif->message.'</p></li>';
    }
}
?>
<footer>
    {!! Form::open(['url' => '/search', 'method' => 'GET']) !!}
    {!! Form::text('search', '', ['placeholder' => 'Найти']) !!}
    {!! Form::close() !!}
    <div class="notifications group">
        <div class="tasks group">
            <div class="favs">
                <a href="#" class="labels">{{favsTasksCount()}}</a>
                <div class="list">
                    <p><b>Задачи</b></p>
                    <ul class="list-unstyled">
                    {{favsTasks()}}
                    </ul>
                </div>
            </div>
            <div class="fires">
                <a href="#" class="labels">{{fireTasksCount()}}</a>
                <div class="list">
                    <p><b>Задачи</b></p>
                    <ul class="list-unstyled">
                        {{firesTasks()}}
                    </ul>
                </div>
            </div>
        </div>
        <div class="messages">
            <div class="nots">
                <a href="#" class="labels">{{notifFirtsCount()}}</a>
                <div class="list">
                    <ul class="list-unstyled">
                        {!! notifFirtsList() !!}
                    </ul>
                </div>
            </div>
            <div class="attans">
                <a href="#" class="labels">{{notifSecondCount()}}</a>
                <div class="list">
                    <ul class="list-unstyled">
                        {!! notifSecondList() !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>