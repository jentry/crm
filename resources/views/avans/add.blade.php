@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/premia')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>{{$user->name}}</h1>

        <div class="managers_list">
            <table class="table table-responsive">
                <tr>
                    <th class="t1"></th>
                    <th class="t2"></th>
                    <th class="t3">Имя</th>
                    <th class="t4">Аванс</th>
                    <th class="t5">Число и месяц</th>
                    <th class="t6">Год</th>
                    <th class="t7"></th>
                </tr>
                @foreach($user->avanses as $avans)
                    <tr>
                        <td class="t1"></td>
                        <td class="t2"><a href="#" data-id="{{$avans->id}}" data-summ="{{$avans->summ}}" class="edit-avans"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                        <td class="t3"><a href="{{url('/manager/public/'.$avans->user->id)}}">{{ $avans->user->name }}</a></td>
                        <td class="t4">{{$avans->summ}} руб.</td>
                        <td class="t5">{{$avans->month}}</td>
                        <td class="t6">{{$avans->year}}</td>
                        <td class="t7"></td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="add_premia">
            <h2>Назначить аванс</h2>
            {!! Form::open(['url' => 'avans/store', 'method' => 'POST']) !!}
            {!! Form::hidden('user', $user->id) !!}
            <p>Сумма аванса</p>
            {!! Form::number('summ', '', ['class' => 'form-control']) !!}
            <p>За какой месяц</p>
            {!! Form::text('data', '', ['id' => 'datetimepicker2', 'class' => 'form-control']) !!}
            <button type="submit" class="btn btn-info">Назначить</button>
            {!! Form::close() !!}
        </div>

    </section>
    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Редактировать аванс</h3>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'avans/update', 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', '') !!}
                    {!! Form::hidden('user', $user->id) !!}
                    {!! Form::number('summ', '', ['class' => 'form-control']) !!}
                    <button type="submit" class="btn btn-info">Редактировать</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection