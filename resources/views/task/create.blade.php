@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/tasks')}}" class="group">Мои задачи <span>{{$all}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>Создать новую задачу</h1>
        {!! Form::open(['url' => 'task/create', 'method' => 'POST', 'files' => true]) !!}
        <p>Название задачи</p>
        {!! Form::text('title', '', array('class' => 'form-control')) !!}
        <p>Суть задачи</p>
        {!! Form::textarea('description', '', array('class' => 'textarea form-control')) !!}
        <div style="position: relative">
        <p>Дедлайн</p>
        {!! Form::text('deadline', '', array('class' => 'form-control', 'id' => 'datetimepicker1')) !!}
        </div>
        <div>
            <p>Выбрать родительскую задачу</p>
            {!! Form::select('parent_id', $tasks, null, ['placeholder' => 'Выберите задачу', 'size' => '1']) !!}
        </div>
        <p>Горит: {!! Form::checkbox('fire', true, false) !!}</p>
        <p>Файлы:</p>
        <input name="files[]" type="file" multiple="multiple" />
        <p><a href="javascript:void(showHideUsers(this));">Ответственный <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
        <br>
        <span></span>
        </p>
        <div class="task_users group" id="users1" style="display: block;">
            <div class="list" id="list1">
                <ul class="list-unstyled">
                    @foreach($users as $user)
                    <li data-type="{{$user->roles[0]->id}}" class="group">

                        <div class="foto"><img src="{{asset('upload/users/'.$user->foto)}}" alt="" class="img-responsive"></div>
                        @if($user->id == $userid)
                            {!! Form::radio('user', $user->id, true, ['id' => 'user'.$user->id]) !!}
                        @else
                            {!! Form::radio('user', $user->id, false, ['id' => 'user'.$user->id]) !!}
                        @endif
                        <div class="name">
                            <label for="user{{$user->id}}">{{$user->name}}</label>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="types" id="types1">
                <ul class="list-unstyled">
                    <li><a href="#" data-id="null" class="active">Все</a></li>
                    @foreach($roles as $role)
                        <li><a href="#" data-id="{{$role->id}}">{{$role->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
<!--        <p><a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> Участники</a></p>-->
        <div class="auditors">
            <div class="adds_users">
                <p class="title">Соисполнители - <a href="#" class="slideDown" data-block="users2">Выбрать <i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>
                <div class="task_users group" id="users2" style="display: none;">
                    <div class="list" id="list2">
                        <ul class="list-unstyled">
                            @foreach($users as $user)
                                <li data-type="{{$user->roles[0]->id}}" class="group">

                                    <div class="foto"><img src="{{asset('upload/users/'.$user->foto)}}" alt="" class="img-responsive"></div>
                                        {!! Form::checkbox('suser[]', $user->id, false, ['id' => 'suser'.$user->id]) !!}
                                    <div class="name">
                                        <label for="suser{{$user->id}}">{{$user->name}}</label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="types" id="types2">
                        <ul class="list-unstyled">
                            <li><a href="#" data-id="null" class="active">Все</a></li>
                            @foreach($roles as $role)
                                <li><a href="#" data-id="{{$role->id}}">{{$role->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="adds_auditor">
                <p class="title">Аудиторы - <a href="#" class="slideDown" data-block="users3">Выбрать <i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>
                <div class="task_users group" id="users3" style="display: none;">
                    <div class="list" id="list3">
                        <ul class="list-unstyled">
                            @foreach($users as $user)
                                <li data-type="{{$user->roles[0]->id}}" class="group">

                                    <div class="foto"><img src="{{asset('upload/users/'.$user->foto)}}" alt="" class="img-responsive"></div>
                                    {!! Form::checkbox('auditor[]', $user->id, false, ['id' => 'auditor'.$user->id]) !!}
                                    <div class="name">
                                        <label for="auditor{{$user->id}}">{{$user->name}}</label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="types" id="types3">
                        <ul class="list-unstyled">
                            <li><a href="#" data-id="null" class="active">Все</a></li>
                            @foreach($roles as $role)
                                <li><a href="#" data-id="{{$role->id}}">{{$role->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttons">
            <button type="submit" class="btn btn-info">Поставить задачу</button>
            <a href="{{url('/tasks')}}" class="btn btn-danger"><i class="fa fa-times-circle" aria-hidden="true"></i> Выйти без сохранения</a>
        </div>
        {!! Form::close() !!}
    </section>
    <script type="text/javascript">
        $(document).ready(function(){
            $('select').styler();
        })
    </script>
@endsection