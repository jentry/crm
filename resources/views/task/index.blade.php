@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li class="active"><a href="{{url('/tasks')}}" class="group">Мои задачи <span>{{$all}}</span></a></li>
            </ul>
            <ul class="list-unstyled inside">
                <li><a href="#" class="group sort_tasks_link" data-type="user">Входящие <span>{{$inCount}}</span></a></li>
                <li><a href="#" class="group sort_tasks_link" data-type="author">Исходящие <span>{{$outCount}}</span></a></li>
            </ul>
            <p class="finished"><a href="#" class="sort_tasks_link" data-type="finished">Завершенные <span>{{$finCount}}</span></a></p>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1 class="pull-left">Мои задачи</h1>
        <a href="{{url('task/add')}}" class="add_new_task pull-right">Поставить задачу</a>
        <div class="clearfix"></div>
        <table class="table table-responsive" id="tasks_table">
            <thead>
                <tr>
                    <th class="t1"></th>
                    <th class="t2">Задача</th>
                    <th class="t3">Дедлайн</th>
                    <th class="t4">Ответственный</th>
                    <th class="t5">Поставновщик</th>
                    <th class="t6"><i class="fa fa-star" aria-hidden="true"></i></th>
                    <th class="t7"></th>
                </tr>
            </thead>
            <tbody id="tbody">
                @foreach($tasks as $task)
                <tr>
                    <td class="t1">
                        @if(count($task->kids))
                            <a href="#" class="clicker" data-id="{{$task->id}}"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                            <small>{{count($task->kids)}}</small>
                        @endif
                    </td>
                    <td class="t2"><a href="{{url('/task/'.$task->id)}}" @if(count($task->kids)) class="large" @endif>{{$task->title}}</a></td>
                    <td class="t3">{{ date( 'd.m.Y', strtotime($task->deadline)) }}</td>
                    <td class="t4">{{ $task->user->name }}</td>
                    <td class="t5">{{ $task->author->name }}</td>
                    <td class="t6">
                    @if($task->favorite == 0)
                        <a href="#" class="fav empty" data-type="add" data-id="{{$task->id}}"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                    @else
                        <a href="#" class="fav full" data-type="remove" data-id="{{$task->id}}"><i class="fa fa-star" aria-hidden="true"></i></a>
                    @endif
                    </td>
                    <td class="t7"></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>
@endsection