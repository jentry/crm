@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <div class="task_options">
            <p><a href="{{url('/tasks')}}">Вернуться к задачам</a></p>
            <div class="times">
                <p>Старт: {{ date( 'd.m.Y', strtotime($task->created_at))  }}</p>
                <p>Дедлайн: {{ date( 'd.m.Y', strtotime($task->deadline))  }}</p>
            </div>
            <p class="percent">Процент завершения: <span>{{$task->percent}}</span>% <a href="#" class="changePercent" data-id="{{$task->id}}"><i class="fa fa-pencil" aria-hidden="true"></i></a></p>
            <div id="changePercent" style="padding-right: 15px;" class="hide">
                <input type="number" name="percents" value="" class="form-control">
                <button type="button" id="changeButton1" class="btn btn-info" data-id="{{$task->id}}">Ок</button>
            </div>
            <p class="status">Статус: <span>{{$task->status}}</span> <a href="#" class="changeStatus" data-id="{{$task->id}}"><i class="fa fa-pencil" aria-hidden="true"></i></a></p>
            <div id="changeStatus" style="padding-right: 15px;" class="hide">
                <select name="status" class="form-control">
                    <option value="Назначена">Назначена</option>
                    <option value="Принята">Принята</option>
                    <option value="В работе">В работе</option>
                    <option value="Закончена">Закончена</option>
                </select>
                <button type="button" id="changeButton2" class="btn btn-info" data-id="{{$task->id}}">Ок</button>
            </div>
            @if(Auth::user()->id == $task->author_id)
                @if($task->finished)
                    <button type="button" class="btn btn-primary" id="changeFinishedButton" data-id="{{$task->id}}" data-state="0">Открыть задачу</button>
                @else
                <button type="button" class="btn btn-primary" id="changeFinishedButton" data-id="{{$task->id}}" data-state="1">Задача завершена</button>
                @endif
            @endif
            <div class="author">
                <p>Постановщик:</p>
                <p><a href="{{url('/manager/public/'.$task->author->id)}}">{{$task->author->name}}</a>
                <br>
                    <small>{{$task->author->otdel}}</small>
                </p>
            </div>
            <div class="user">
                <p>Исполнитель:</p>
                <p><a href="{{url('/manager/public/'.$task->user->id)}}">{{$task->user->name}}</a>
                    <br>
                    <small>{{$task->user->otdel}}</small>
                </p>
            </div>
            <div class="susers">
                @if(count($susers))
                    <p>Соисполнители:</p>
                    @foreach($susers as $suser)
                        <p><a href="{{url('/manager/public/'.$suser->id)}}">{{$suser->name}}</a>
                            <br>
                            <small>{{$suser->otdel}}</small>
                        </p>
                    @endforeach
                    <a href="#" class="change" id="changeSusers" data-id="{{$task->id}}">Сменить соисполнителей</a>
                @else
                    <a href="#" class="change" id="changeSusers" data-id="{{$task->id}}">Добавить соисполнителей</a>
                @endif
            </div>
            <div class="auditors">
                @if(count($auditors))
                <p>Аудиторы:</p>
                    @foreach($auditors as $auditor)
                        <p><a href="{{url('/manager/public/'.$auditor->id)}}">{{$auditor->name}}</a>
                            <br>
                            <small>{{$auditor->otdel}}</small>
                        </p>
                    @endforeach
                    <a href="#" class="change" id="changeAuditors" data-id="{{$task->id}}">Сменить аудиторов</a>
                @else
                    <a href="#" class="change" id="changeAuditors" data-id="{{$task->id}}">Добавить аудиторов</a>
                @endif
            </div>
        </div>

    </aside>
    <section class="main col-xs-10" id="single_task">
        <h1 class="pull-left">{{$task->title}} @if($task->finished) (<small>Задача завершена</small>)@endif</h1>
        @if($task->favorite == 0)
        <a href="#" class="pull-left col-xs-offset-1 fav empty" data-type="add" data-id="{{$task->id}}"><i class="fa fa-star-o" aria-hidden="true"></i></a>
        @else
            <a href="#" class="pull-left col-xs-offset-1 fav full" data-type="remove" data-id="{{$task->id}}"><i class="fa fa-star" aria-hidden="true"></i></a>
        @endif
        <div class="clearfix"></div>
        <div class="description group" style="margin-top: 10px;">
            <div class="col-xs-9">
                <p><b>Суть задачи:</b></p>
                {!! $task->description !!}
            </div>
            <div class="col-xs-3">
                @if(count($files))
                <p><b>Файлы:</b></p>
                @foreach($files as $file)
                    <p><a href="{{URL::to('/upload/tasks/'.$file->file_name)}}">{{$file->file_name}}</a></p>
                @endforeach
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        <p class="edit"><a href="{{url('task/edit/'.$task->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i> Редактировать задачу</a></p>
        @if(count($task->kids))
            <h2>Подзадачи</h2>
            <table class="table table-responsive" id="tasks_table">
                <thead>
                <tr>
                    <th class="t1"></th>
                    <th class="t2">Задача</th>
                    <th class="t3">Дедлайн</th>
                    <th class="t4">Ответственный</th>
                    <th class="t5">Поставновщик</th>
                    <th class="t6"><i class="fa fa-star" aria-hidden="true"></i></th>
                    <th class="t7"></th>
                </tr>
                </thead>
                <tbody id="tbody">
                @foreach($task->kids as $t)
                    <tr>
                        <td class="t1">
                            @if(count($t->kids))
                                <a href="#" class="clicker" data-id="{{$t->id}}"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                                <small>{{count($t->kids)}}</small>
                            @endif
                        </td>
                        <td class="t2"><a href="{{url('/task/'.$t->id)}}" @if(count($t->kids)) class="large" @endif>{{$t->title}}</a></td>
                        <td class="t3">{{ date( 'd.m.Y', strtotime($t->deadline)) }}</td>
                        <td class="t4">{{ $t->user->name }}</td>
                        <td class="t5">{{ $t->author->name }}</td>
                        <td class="t6">
                            @if($t->favorite == 0)
                                <a href="#" class="fav empty" data-type="add" data-id="{{$t->id}}"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                            @else
                                <a href="#" class="fav full" data-type="remove" data-id="{{$t->id}}"><i class="fa fa-star" aria-hidden="true"></i></a>
                            @endif
                        </td>
                        <td class="t7"></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        @if(count($task->comments))
            <h3>Комментарии к задаче:</h3>
            @foreach($task->comments as $comment)
                <div class="one_comment group">
                    <div class="foto">
                        <img src="{{asset('upload/users/'.$comment->user->foto)}}" alt="" class="img-responsive">
                    </div>
                    <div class="content">
                        <p class="name">{{$comment->user->name}}</p>
                        <p class="options">{{date( 'd.m.Y H:i', strtotime($comment->created_at))}}
                            <a href="#" class="edit_comment" data-id="{{$comment->id}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        </p>
                        <p>{{$comment->text}}</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach
        @endif
        <h4>Добавить комментарий:</h4>
        {!! Form::open(array('url' => '/taskcomment/add', 'method' => 'POST', 'files' => true, 'id' => 'commentForm')) !!}
            {!! Form::hidden('id', $task->id) !!}
            {!! Form::textarea('comment', '', ['placeholder' => 'Ваш комментарий', 'class' => 'form-control']) !!}
            <div style="margin-bottom: 25px;">
                {!! Form::file('files', '', array('class' => 'form-control', 'multiple' => 'multiple')) !!}
            </div>
            <button type="submit" class="btn btn-info">Отправить</button>
        {!! Form::close() !!}


        <div class="modal fade" id="commentsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Редактировать комментарий</h3>
                    </div>
                    <div class="modal-body" style="height:450px; overflow: auto;">

                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Редактировать соисполнителей</h3>
                    </div>
                    <div class="modal-body" style="height:450px; overflow: auto;">

                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Редактировать аудиторов</h3>
                    </div>
                    <div class="modal-body" style="height:450px; overflow: auto;">

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection