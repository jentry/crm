@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/premia')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>{{$user->name}}</h1>

        <div class="managers_list">
            <table class="table table-responsive">
                <tr>
                    <th class="t1"></th>
                    <th class="t2"></th>
                    <th class="t3">Имя</th>
                    <th class="t4">Премия</th>
                    <th class="t5">Число и месяц</th>
                    <th class="t6">Год</th>
                    <th class="t7"></th>
                </tr>
                @foreach($user->premias as $premia)
                    <tr>
                        <td class="t1"></td>
                        <td class="t2"><a href="#" data-id="{{$premia->id}}" data-summ="{{$premia->summ}}" class="edit-premia"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                        <td class="t3"><a href="{{url('/manager/public/'.$premia->user->id)}}">{{ $premia->user->name }}</a></td>
                        <td class="t4">{{$premia->summ}} руб.</td>
                        <td class="t5">{{$premia->day}} {{$premia->month}}</td>
                        <td class="t6">{{$premia->year}}</td>
                        <td class="t7"></td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="add_premia">
            <h2>Добавить премию</h2>
            {!! Form::open(['url' => 'premia/store', 'method' => 'POST']) !!}
            {!! Form::hidden('user', $user->id) !!}
            <p>Сумма премии</p>
            {!! Form::number('summ', '', ['class' => 'form-control']) !!}
            <p>За какой день</p>
            {!! Form::text('data', '', ['id' => 'datetimepicker1', 'class' => 'form-control']) !!}
            <button type="submit" class="btn btn-info">Назначить</button>
            {!! Form::close() !!}
        </div>

    </section>
    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Редактировать премию</h3>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'premia/update', 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', '') !!}
                    {!! Form::hidden('user', $user->id) !!}
                    {!! Form::number('summ', '', ['class' => 'form-control']) !!}
                    <button type="submit" class="btn btn-info">Редактировать</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection