@extends('layout.layout')

@section('content')
    <script type="text/javascript" src="{{asset('js/calendar.js')}}"></script>
    <script type="text/javascript">
        var _data = 'тест';

        function setData(s){
            _data = s;
            return _data;
        }

        function getData(id, month, year){
            var _month = month -1;
            var _dd = new Date(year, month-1);
            var _days = [];
            while (_dd.getMonth() == _month){
                var _day = _dd.getDate();
                _days.push(_day);

                _dd.setDate(_dd.getDate() + 1);
            }

            var $_token = $('meta[name="csrf-token"]').attr('content');

             $.ajax({
                 type: "POST",
                 url: "/salary/foruser",
                 headers: { 'X-CSRF-TOKEN' : $_token },
                 data: {userId : id, days : _days, month : month, year : year},
                 beforeSend: function(){

                 },
                 success: function(data){

                     setData(data);
                     renderCalendar(year, month, id);
                 },
                 error: function(data){
                    console.log(data.responseText);
                 }
             });

        }

        function getDay(date){
            var day = date.getDay();
            if(day == 0){
                day = 7;
            }
            return day - 1;
        }

        function salaryMonth(id, month, year){
            var _month = month -1;
            var _dd = new Date(year, month-1);
            var _daysCount = 0;
            var _allDaysCount = 0;
            var _days = [];
            while (_dd.getMonth() == _month){
                if(getDay(_dd) != 5 && getDay(_dd) != 6){
                    _daysCount = _daysCount + 1;
                }
                _allDaysCount = _allDaysCount + 1;

                var _day = _dd.getDate();
                _days.push(_day);
                _dd.setDate(_dd.getDate() + 1);
            }

            var $_token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: "POST",
                url: "/salary/monthsalary",
                headers: { 'X-CSRF-TOKEN' : $_token },
                data: {userId : id, daysCount : _daysCount, allDaysCount : _allDaysCount, month : month, year : year, days : _days},
                beforeSend: function(){

                },
                success: function(data){
                    //console.log(data);
                    $.views.settings.delimiters("{%","%}");
                    var tmpl = $.templates("#salaryTable");
                    var _renderData = {
                        name : data.name,
                        oklad : data.oklad,
                        allDaysCount : data.allDaysCount,
                        jobDays : data.jobDays,
                        factSalary : data.factSalary,
                        shtraf : data.shtraf,
                        shtrafSumm : data.shtrafSumm,
                        avans : data.avans,
                        premia : data.premia,
                        result : data.result
                    };
                    var html = tmpl.render(_renderData);
                    $("#user_pay_table").html(html);
                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }

        $(document).ready(function(){
            getData({{Auth::user()->id}}, {{date('n')}}, {{date('Y')}});
            salaryMonth({{Auth::user()->id}}, {{date('n')}}, {{date('Y')}});

            var _month, _year, _newMonth, _newYear, _span = $('.month_head span'),
                    _monthes = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
            $('#month_prev').on('click', function(e){
                e.preventDefault();
                _month = parseInt(_span.attr('data-month'));
                _year = parseInt(_span.attr('data-year'));

                if(_month > 1){
                    _newMonth = _month - 1;
                    _newYear = _year;
                }
                else{
                    _newMonth = 12;
                    _newYear = _year - 1;
                }

                _span.attr({
                    'data-month' : _newMonth,
                    'data-year' : _newYear
                }).text(_monthes[_newMonth-1]+' '+_newYear);

                //renderCalendar(_newYear, _newMonth);
                getData({{Auth::user()->id}}, _newMonth, _newYear);
                salaryMonth({{Auth::user()->id}}, _newMonth, _newYear);
            });

            $('#month_next').on('click', function(e){
                e.preventDefault();
                _month = parseInt(_span.attr('data-month'));
                _year = parseInt(_span.attr('data-year'));

                if(_month < 12){
                    _newMonth = _month + 1;
                    _newYear = _year;
                }
                else{
                    _newMonth = 1;
                    _newYear = _year + 1;
                }

                _span.attr({
                    'data-month' : _newMonth,
                    'data-year' : _newYear
                }).text(_monthes[_newMonth-1]+' '+_newYear);

                getData({{Auth::user()->id}}, _newMonth, _newYear);
                salaryMonth({{Auth::user()->id}}, _newMonth, _newYear);
            });
        });
    </script>
    <div class="salary_page">
        <div class="text">
            <p>Выплаты заработной платы осуществляется два раза в месяц:</p>
            <ul class="list-unstyled">
                <li>- в последний рабочий день текущего месяца аванс в размере 5 000 руб;</li>
                <li>- на десятый рабочий день следующего месяца вся сумма оклада и всех премиальных за вычета аванса;</li>
            </ul>
        </div>
        <div class="month_head">
            <a href="#" id="month_prev"></a>
            <span data-month="{{date('n')}}" data-year="{{date('Y')}}">{{$month}} {{date('Y')}}</span>
            <a href="#" id="month_next"></a>
        </div>
        <div id="calendarApp">
            <table>
                <thead>
                    <tr>
                        <th>Пн</th>
                        <th>Вт</th>
                        <th>Ср</th>
                        <th>Чт</th>
                        <th>Пт</th>
                        <th class="holiday">Сб</th>
                        <th class="holiday">Вс</th>
                    </tr>
                </thead>
                <tbody id="calendar">

                </tbody>
            </table>
        </div>
        <div id="user_pay_table">
            <!--<table>
                <tr>
                    <th class="t1"></th>
                    <th class="t2">ФИО</th>
                    <th class="t3">Оклад</th>
                    <th class="t4">Дни мес</th>
                    <th class="t5">Дни раб</th>
                    <th class="t6">Факт ЗП</th>
                    <th class="t7">Мин Штр</th>
                    <th class="t8">Штрафы</th>
                    <th class="t9">Аванс</th>
                    <th class="t10">Премия</th>
                    <th class="t11">Итого</th>
                    <th class="t12"></th>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ Auth::user()->name }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>-->
        </div>
    </div>
@endsection