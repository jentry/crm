@extends('layout.layout')

@section('content')
    <section class="clients group" style="position: relative;">
        <div class="loading">
            <div class="like_table">
                <div class="table_cell">
                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                </div>
            </div>

        </div>
        <div class="left">
            <div class="new_clients">
                <h1>Новый клиент</h1>
                <p>Перед звонком обязательно ознакомьтесь с характеристиками сайта.</p>
                <div class="form">
                    {!! Form::open(array('url' => '/client/search', 'method' => 'POST', 'class' => 'group', 'id' => 'searchForm')) !!}
                        {!! Form::text('site', '', array('placeholder' => 'http://www.sitename.ru'))  !!}
                        {!! Form::hidden('zapros', '', array('placeholder' => 'запрос для поиска'))  !!}
                        <button type="submit">Искать</button>
                        <button type="button" id="toBlacklist">Blacklist</button>
                    {!! Form::close() !!}
                    <p class="results">Нашел сайт по запросу «<span class="zap"></span>» Яндекс - <span class="pos"></span> позиция
                    <a href="#" id="likeButton"><img src="{{asset('images/like.png')}}" alt="" id="like"></a>
                        <a href="#" id="dislikeButton"><img src="{{asset('images/dislike.png')}}" alt="" id="dislike"></a>
                    </p>
                </div>
            </div>
            @if(Auth::user()->is('admin'))
            <div class="file_block">
                <h2>Загрузить файл с запросами</h2>
                {!! Form::open(array('url' => '/client/file', 'method' => 'POST', 'id' => 'fileForm', 'files' => true)) !!}
                <p>Файл с запросами</p>
                {!! Form::file('file') !!}
                <p></p>
                <button type="submit" class="btn btn-info">Загрузить</button>
                {!! Form::close() !!}
            </div>
            @endif
            <div class="comments_block">
                <div class="toggle">
                    <a href="#" class="active" data-type="like">Положительные</a>
                    <a href="#" data-type="dislike">Отрицательные</a>
                    <a href="#" data-type="blacklist">Blacklist</a>
                    <a href="#" data-type="project">В проект</a>
                </div>
                <div class="heading">
                    <table>
                        <tr>
                            <th class="t1"></th>
                            <th class="t2">Сайт</th>
                            <th class="t3">Дедлайн</th>
                            <th class="t3">Стадия</th>
                            <th class="t4">Комментарии</th>
                            <th class="t5"></th>
                        </tr>
                    </table>
                </div>
                <div class="content">
					@if($clients)
                    <table id="clientsTable">
                        @foreach($clients as $client)
                        <tr>
                            <td class="t1"><a href="#" data-id="{{$client->id}}" data-type="full" class="full">{{$client->name}}</a></td>
                            <td class="t2">
                                <div class="relative">
                                    <a href="#" data-id="{{$client->id}}" data-type="deadline" class="deadline">{{$client->deadline}}</a>
                                </div>
                            </td>
                            <td class="t2"><a href="#" data-id="{{$client->id}}" data-type="status" class="status">{{$client->status}}</a></td>
                            <td class="t3">
                                <a href="#" data-id="{{$client->id}}" class="comments">
                                <?php
                                    $commentArray = $client->comments->toArray();
                                    if(!empty($commentArray)){
                                    $commKey = count($commentArray) - 1;
                                    $comm = $commentArray[$commKey];
                                    echo $comm['text'];
                                    } elseif($client->comment != ''){
                                        echo $client->comment;
                                    } else{
                                        echo 'написать комментарий';
                                    }
                                    ?>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </table>
					@endif
                </div>
            </div>
        </div>
        <div class="fordatetimepicker">
            <input type="text" name="d" value="" class="form-control">
        </div>
        <div class="forchangestatus"></div>
        <div class="right">
            <div class="site_info">
                <p class="head">Характеристики сайта</p>
                {!! Form::open() !!}
                {!! Form::text('site_url', '', ['placeholder' => 'Адрес сайта']) !!}
                <button type="submit">Анализировать</button>
                {!! Form::close() !!}
                <div class="content" id="infoContent">
                    <div class="loader"></div>
                    <table>
                        <tr>
                            <th class="t1"></th>
                            <th class="t2">
                                sitename.ru
                                <span>сайт клиента</span>
                            </th>
                            <th class="t3">
                                sitename.ru
                                <span>топ 1</span>
                            </th>
                            <th class="t4">
                                sitename.ru
                                <span>топ 2</span>
                            </th>
                            <th class="t5">
                                sitename.ru
                                <span>топ 3</span>
                            </th>
                        </tr>
                        <tr>
                            <td class="t1">
                                <p class="title">Показатели Яндекса</p>
                                <p>Яндекс тИЦ</p>
                                <p>Яндекс Каталог</p>
                                <p>Проиндексировано</p>
                            </td>
                            <td class="t2">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                            <td class="t3">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                            <td class="t4">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                            <td class="t4">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="t1">
                                <p class="title">Показатели Google</p>
                                <p>Google Page Rank</p>
                                <p>Dmoz.org каталог</p>
                                <p>Проиндексировано</p>
                            </td>
                            <td class="t2">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                            <td class="t3">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                            <td class="t4">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                            <td class="t4">
                                <p class="title"></p>
                                <p></p>
                                <p></p>
                                <p></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="faq">
                <p class="head">F.A.Q.</p>
                <div class="content">
                    <ul class="bxslider list-unstyled group">
                        @foreach($faqs as $faq)
                        <li>
                            <p class="title">{{$faq->title}}</p>
                            {!! $faq->text !!}
                            <div class="hide">
                                <audio controls>
                                    <source src="/upload/audio/{{$faq->audio}}" type="audio/mpeg">
                                </audio>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <a href="#" id="playAudio"></a>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Добавить сайт в положительные</h3>
                </div>
                <div class="modal-body">
                    <form id="addYesForm">
                        <input type="text" name="site" value="" placeholder="Название сайта" class="form-control" style="margin-bottom: 15px;">
                        <input type="text" name="deadline" value="" placeholder="Дедлайн" class="form-control" style="margin-bottom: 15px;" id="datetimepicker1">
                        <select name="stadia" class="form-control" style="margin-bottom: 15px;">
                            <option value="Созвон">Созвон</option>
                            <option value="КП">КП</option>
                            <option value="Встреча">Встреча</option>
                            <option value="Деньги">Деньги</option>
                            <option value="В проекты">В проекты</option>
                        </select>
                        <textarea name="comment", placeholder="Комментарий к проекту" class="form-control" style="margin-bottom: 15px;"></textarea>
                        <button type="submit" class="btn btn-info">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Добавить сайт в отрицательные</h3>
                </div>
                <div class="modal-body">
                    <form id="addNoForm">
                        <input type="text" name="site" value="" placeholder="Название сайта" class="form-control" style="margin-bottom: 15px;">
                        <textarea name="comment" placeholder="Комментарий к проекту" class="form-control" style="margin-bottom: 15px;"></textarea>
                        <button type="submit" class="btn btn-info">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Добавить сайт в blacklist</h3>
                </div>
                <div class="modal-body">
                    <form id="addBlackForm">
                        <input type="text" name="site" value="" placeholder="Название сайта" class="form-control" style="margin-bottom: 15px;">
                        <textarea name="comment", placeholder="Комментарий к проекту" class="form-control" style="margin-bottom: 15px;"></textarea>
                        <button type="submit" class="btn btn-info">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Редактировать</h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="commentsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Комментарии</h3>
                </div>
                <div class="modal-body" style="height:450px; overflow: auto;">

                </div>
                <div class="modal-footer" style="text-align: left">
                    <form id="addCommentForm">
                        <textarea name="comment" placeholder="Комментарий" class="form-control"></textarea>
                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn btn-info">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Редактировать дедлайн</h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <!---->
    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Редактировать статус</h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
@endsection