@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/daysalary')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>{{$user->name}}</h1>

        <div class="managers_list">
            <table class="table table-responsive">
                <tr>
                    <th class="t1"></th>
                    <th class="t2"></th>
                    <th class="t3">Имя</th>
                    <th class="t4">Сумма</th>
                    <th class="t5">День</th>
                    <th class="t7"></th>
                </tr>
                @foreach($user->daysalaries as $day)
                    <tr>
                        <td class="t1"></td>
                        <td class="t2"><a href="#" data-id="{{$day->id}}" data-summ="{{$day->summ}}" class="edit-daysalary"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                        <td class="t3"><a href="{{url('/manager/public/'.$day->user->id)}}">{{ $day->user->name }}</a></td>
                        <td class="t4">{{$day->summ}} руб.</td>
                        <td class="t5">{{date('d-m-Y', strtotime($day->date))}}</td>
                        <td class="t7"></td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="add_premia">
            <h2>Назначить зарплату за день</h2>
            {!! Form::open(['url' => 'daysalary/store', 'method' => 'POST']) !!}
            {!! Form::hidden('user', $user->id) !!}
            <p>Сумма зарплаты за день</p>
            {!! Form::number('summ', '', ['class' => 'form-control']) !!}
            <p>Выбрать день</p>
            {!! Form::text('data', '', ['id' => 'datetimepicker1', 'class' => 'form-control']) !!}
            <button type="submit" class="btn btn-info">Назначить</button>
            {!! Form::close() !!}
        </div>

    </section>
    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Редактировать зарплату за день</h3>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'daysalary/update', 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', '') !!}
                    {!! Form::hidden('user', $user->id) !!}
                    {!! Form::number('summ', '', ['class' => 'form-control']) !!}
                    <button type="submit" class="btn btn-info">Редактировать</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection