@foreach($clients as $client)
<tr>
    <td class="t1"><a href="#" data-id="{{$client->id}}" data-type="full" class="full">{{$client->name}}</a></td>
    <td class="t2"><a href="#" data-id="{{$client->id}}" data-type="deadline" class="deadline">{{$client->deadline}}</a></td>
    <td class="t2"><a href="#" data-id="{{$client->id}}" data-type="status" class="status">{{$client->status}}</a></td>
    <td class="t3">
        <a href="#" data-id="{{$client->id}}" class="comments">
            <?php
            $commentArray = $client->comments->toArray();
            if(!empty($commentArray)){
                $commKey = count($commentArray) - 1;
                $comm = $commentArray[$commKey];
                echo $comm['text'];
            } elseif($client->comment != ''){
                echo $client->comment;
            } else{
                echo '�������� �����������';
            }
            ?>
        </a>
    </td>
</tr>
@endforeach