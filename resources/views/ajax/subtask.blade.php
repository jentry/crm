@foreach($task->kids as $kid)
<tr class="child{{$task->id}} child">
    <td class="t1">
        @if(count($kid->kids))
            <a href="#" class="clicker" data-id="{{$kid->id}}"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
            <small>{{count($kid->kids)}}</small>
        @endif
    </td>
    <td class="t2"><a href="{{url('/task/'.$kid->id)}}">{{$kid->title}}</a></td>
    <td class="t3">{{ date( 'd.m.Y', strtotime($kid->deadline)) }}</td>
    <td class="t4">{{ $kid->user->name }}</td>
    <td class="t5">{{ $kid->author->name }}</td>
    <td class="t6">
        @if($kid->favorite == 0)
            <a href="#" class="fav empty" data-type="add" data-id="{{$kid->id}}"><i class="fa fa-star-o" aria-hidden="true"></i></a>
        @else
            <a href="#" class="fav full" data-type="remove" data-id="{{$kid->id}}"><i class="fa fa-star" aria-hidden="true"></i></a>
        @endif
    </td>
    <td class="t7"></td>
</tr>
@endforeach