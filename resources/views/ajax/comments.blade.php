
@foreach($comments as $comment)
<div style="padding: 10px;" class="modalComment">
    <p>{!! $comment->text !!}</p>
    <p>Автор: {{$comment->user->name}}</p>
    <p>{{ date( 'd.m.Y H:i', strtotime($comment->created_at)) }}</p>
</div>
@endforeach