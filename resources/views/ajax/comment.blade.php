{!! Form::open(array('url' => '/taskcomment/edit', 'method' => 'PUT', 'files' => true, 'id' => 'commentEditForm')) !!}
    {!! Form::hidden('id', $comment->task->id) !!}
    {!! Form::textarea('comment', $comment->text, ['placeholder' => 'Ваш комментарий', 'class' => 'form-control']) !!}
    <div style="margin-bottom: 25px;">
        {!! Form::file('files', '', array('class' => 'form-control', 'multiple' => 'multiple')) !!}
    </div>
    <button type="submit" class="btn btn-info">Сохранить</button>
{!! Form::close() !!}