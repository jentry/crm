<form id="editForm">
    <input type="text" name="site" value="{{$client->name}}" class="form-control" style="margin-bottom: 15px;">
    <input type="hidden" name="id" value="{{$client->id}}">

    @if((int)$client->islike == 1)
    <input type="text" name="deadline" value="{{$client->deadline}}" placeholder="Дедлайн" class="form-control datetimepicker" style="margin-bottom: 15px;">
    <select name="stadia" class="form-control" style="margin-bottom: 15px;">
        <option value="Созвон">Созвон</option>
        <option value="КП">КП</option>
        <option value="Встреча">Встреча</option>
        <option value="Деньги">Деньги</option>
        <option value="В проекты">В проекты</option>
    </select>
    @endif
    <p>Добавить кооментарий</p>
    <textarea name="comment", class="form-control" style="margin-bottom: 15px;"></textarea>
    <button type="submit" class="btn btn-info">Редактировать</button>

</form>