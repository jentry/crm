{!! Form::open(['url' => 'susers/update', 'method' => 'PUT']) !!}
{!! Form::hidden('id', $id) !!}
<div class="task_users group" id="users1" style="margin-bottom: 15px;">
    <div class="list" id="list1">
        <ul class="list-unstyled">
            @foreach($users as $user)
                <li data-type="{{$user->roles[0]->id}}" class="group">

                    <div class="foto"><img src="{{asset('upload/users/'.$user->foto)}}" alt="" class="img-responsive"></div>
                    @if(in_array($user->id, $susers))
                        {!! Form::checkbox('suser[]', $user->id, true, ['id' => 'user'.$user->id]) !!}
                    @else
                        {!! Form::checkbox('suser[]', $user->id, false, ['id' => 'user'.$user->id]) !!}
                    @endif
                    <div class="name">
                        <label for="user{{$user->id}}">{{$user->name}}</label>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="types" id="types1">
        <ul class="list-unstyled">
            <li><a href="#" data-id="null" class="active">Все</a></li>
            @foreach($roles as $role)
                <li><a href="#" data-id="{{$role->id}}">{{$role->name}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
<button type="submit" class="btn btn-info">Сохранить</button>
{!! Form::close() !!}