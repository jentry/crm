@foreach($tasks as $task)
    <tr>
        <td class="t1">
            @if(count($task->kids))
                <a href="#" class="clicker" data-id="{{$task->id}}"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                <small>{{count($task->kids)}}</small>
            @endif
        </td>
        <td class="t2"><a href="{{url('/task/'.$task->id)}}" @if(count($task->kids)) class="large" @endif>{{$task->title}}</a></td>
        <td class="t3">{{ date( 'd.m.Y', strtotime($task->deadline)) }}</td>
        <td class="t4">{{ $task->user->name }}</td>
        <td class="t5">{{ $task->author->name }}</td>
        <td class="t6">
            @if($task->favorite == 0)
                <a href="#" class="fav empty" data-type="add" data-id="{{$task->id}}"><i class="fa fa-star-o" aria-hidden="true"></i></a>
            @else
                <a href="#" class="fav full" data-type="remove" data-id="{{$task->id}}"><i class="fa fa-star" aria-hidden="true"></i></a>
            @endif
        </td>
        <td class="t7"></td>
    </tr>
@endforeach