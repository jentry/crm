@extends('layouts.app')

@section('content')
    <link href="{{asset('css/screen.css')}}" type="text/css" rel="stylesheet">
    <section class="main">
        <h1>Вы не можете пользоваться системой. Обратитесь к администратору для уточнения деталей.</h1>
        <p><a href="mailto:{{$admin->email}}">Написать администратору.</a></p>
    </section>

@endsection