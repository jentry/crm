@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/managers')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>{{$user->name}}</h1>
        <div class="public_profile group">
            <div class="foto">
                @if($user->foto != '')
                <img src="{{asset('/upload/users/'.$user->foto)}}" alt="" class="img-responsive">
                @endif
            </div>
            <div class="contact_info">
                <div class="links">
                    <a href="{{url('/task/add/'.$user->id)}}" class="add_task"><i class="fa fa-plus-circle" aria-hidden="true"></i> Поставить задачу</a>
                    <a href="{{url('/message/add/'.$user->id)}}" class="add_message"><i class="fa fa-envelope-o" aria-hidden="true"></i> Написать</a>
                </div>
                <div class="foto">
                    @if($user->foto != '')
                        <img src="{{asset('/upload/users/'.$user->foto)}}" alt="" class="img-responsive">
                    @endif
                </div>
                <p>Телефон: <span>{{$user->phone}}</span></p>
                <p>Почта: <a href="mailto:{{$user->email}}">{{$user->email}}</a></p>
                <p>Скайп: <span>{{$user->skype}}</span></p>
                <p>Режим работы: <span>{{$user->worktime}}</span></p>
                <p>Год рождения: <span>{{$user->bornyear}} г.</span></p>
                <?php
                $d = new DateTime($user->created_at);
                ?>
                <p>Работает с: <span>{{$d->format('d.m.Y')}}</span></p>
                <p>Непрочитанных уведомлений нет</p>
            </div>
        </div>
    </section>

@endsection