@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/managers')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        @if(Auth::user()->is('admin'))
        <h1>Добавить пользователя</h1>
        <div class="profile_form">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('url' => '/manager/add', 'method' => 'POST', 'files' => true)) !!}
            <p>Имя</p>
            {!! Form::text('name', '', array('class' => 'form-control')) !!}
            <p>E-mail</p>
            {!! Form::email('email', '', array('class' => 'form-control')) !!}
            <p>Пароль</p>
            {!! Form::password('password', array('class' => 'form-control')) !!}
            <p>Отдел и должность</p>
            {!! Form::text('otdel', '', array('class' => 'form-control')) !!}
            <p>Телефон</p>
            {!! Form::tel('phone', '', array('class' => 'form-control')) !!}
            <p>Skype</p>
            {!! Form::text('skype', '', array('class' => 'form-control')) !!}
            <p>Время работы</p>
            {!! Form::text('worktime', '', array('class' => 'form-control')) !!}
            <p>Год рождения</p>
            {!! Form::text('bornyear', '', array('class' => 'form-control')) !!}
            <p>Роль пользователя:</p>
            @foreach($roles as $role)
                <label for="role{{$role->id}}">{{$role->name}}</label>
                <input type="radio" name="role" value="{{$role->id}}" id="role{{$role->id}}">
                <br>
            @endforeach
            <p>Зарплата в час, руб.</p>
            {!! Form::text('salary', '', array('class' => 'form-control')) !!}
            <p>Фотография</p>
            {!! Form::file('foto', '', array('class' => 'form-control')) !!}
            <button type="submit" class="btn btn-info" style="margin-top:20px;">Сохранить</button>
            {!! Form::close() !!}
        </div>
        @else
        <h1>Вы не можете создавать пользователей</h1>
        @endif
    </section>

@endsection