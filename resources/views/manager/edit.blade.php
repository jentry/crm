@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/managers')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        @if(Auth::user()->is('admin'))
            <h1>Редактировать пользователя {{$manager->name}}</h1>
            <div class="profile_form">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('url' => '/manager/adminedit', 'method' => 'PUT', 'files' => true)) !!}
                {!! Form::hidden('id', $manager->id) !!}
                <p>Имя</p>
                {!! Form::text('name', $manager->name, array('class' => 'form-control')) !!}
                <p>E-mail</p>
                {!! Form::email('email', $manager->email, array('class' => 'form-control')) !!}
                <p>Пароль (чтоб не менять - оставить пустым)</p>
                {!! Form::password('password', array('class' => 'form-control')) !!}
                <p>Отдел и должность</p>
                {!! Form::text('otdel', $manager->otdel, array('class' => 'form-control')) !!}
                <p>Телефон</p>
                {!! Form::tel('phone', $manager->phone, array('class' => 'form-control')) !!}
                <p>Skype</p>
                {!! Form::text('skype', $manager->skype, array('class' => 'form-control')) !!}
                <p>Время работы</p>
                {!! Form::text('worktime', $manager->worktime, array('class' => 'form-control')) !!}
                <p>Год рождения</p>
                {!! Form::text('bornyear', $manager->bornyear, array('class' => 'form-control')) !!}
                <p>Роль пользователя:</p>
                @foreach($roles as $role)
                    <label for="role{{$role->id}}">{{$role->name}}</label>
                    <input type="radio" name="role" value="{{$role->id}}" id="role{{$role->id}}" <?php if($manager->roles[0]->id == $role->id){ ?> checked <?php }?>>
                    <br>
                @endforeach
                <p>Зарплата в час, руб.</p>
                {!! Form::text('salary', $manager->salary, array('class' => 'form-control')) !!}
                <button type="submit" class="btn btn-info" style="margin-top:20px;">Редактировать</button>
                {!! Form::close() !!}
                <p></p>
                @if($manager->active)
                    {!! Form::open(['url' => '/manager/disable', 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', $manager->id) !!}
                    {!! Form::hidden('status', 0) !!}
                    <button type="submit" class="btn btn-danger">Деактивировать пользователя</button>
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['url' => '/manager/disable', 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', $manager->id) !!}
                    {!! Form::hidden('status', 1) !!}
                    <button type="submit" class="btn btn-success">Активировать пользователя</button>
                    {!! Form::close() !!}
                @endif
                <p></p>
                {!! Form::open(['url' => '/manager/destroy', 'method' => 'DELETE']) !!}
                {!! Form::hidden('id', $manager->id) !!}
                <button type="submit" class="btn btn-danger">Удалить пользователя</button>
                {!! Form::close() !!}
            </div>
        @else
            <h1>Вы не можете редактировать пользователей</h1>
        @endif
    </section>

@endsection