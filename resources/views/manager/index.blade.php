@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/managers')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>Сотрудники компании</h1>
        @if(Auth::user()->is('admin'))
        <p><a href="{{url('/manager/add')}}">Добавить сотрудника</a></p>
        @endif

        <div class="managers_list">
        <table class="table table-responsive">
            <tr>
                <th class="t1"></th>
                <th class="t2"></th>
                <th class="t3">Имя</th>
                <th class="t4">Должность</th>
                <th class="t5">Почта</th>
                <th class="t6">Телефон</th>
                <th class="t7"></th>
            </tr>
            @foreach($users as $user)
            <tr>
                <td class="t1"></td>
                <td class="t2">
                    @if($user->foto != '')
                    <img src="{{asset('/upload/users/'.$user->foto)}}" alt="" class="img-responsive center-block">
                    @endif
                </td>
                <td class="t3">
                    <a href="{{url('/manager/public/'.$user->id)}}">{{ $user->name }}</a>
                    @if(Auth::user()->is('admin'))
                        <br>
                        (<a href="{{url('/manager/adminedit/'.$user->id)}}">Редактировать сотрудника</a>)
                    @endif
                    @if($user->active == false)
                        <p style="color:#cccccc;">Пользователь неактивен</p>
                    @endif
                </td>
                <td class="t4">{{ $user->otdel }}</td>
                <td class="t5">{{ $user->email }}</td>
                <td class="t6">{{ $user->phone }}</td>
                <td class="t7"></td>
            </tr>
            @endforeach
        </table>
        </div>
    </section>

@endsection