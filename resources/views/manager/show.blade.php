@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <ul class="list-unstyled">
                <li><a href="{{url('/managers')}}">Все сотрудники <span>{{$count}}</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>{{$user->name}} - Редактировать профиль</h1>
        <div class="profile_form">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('url' => '/manager/edit', 'method' => 'PUT', 'files' => true)) !!}
                <p>Имя</p>
                {!! Form::text('name', $user->name, array('class' => 'form-control')) !!}
            <p>E-mail</p>
            {!! Form::email('email', $user->email, array('class' => 'form-control')) !!}
            <p>Пароль (чтоб не менять - оставить пустым)</p>
            {!! Form::password('password', array('class' => 'form-control')) !!}
            <p>Отдел и должность</p>
            {!! Form::text('otdel', $user->otdel, array('class' => 'form-control')) !!}
            <p>Телефон</p>
            {!! Form::tel('phone', $user->phone, array('class' => 'form-control')) !!}
            <p>Skype</p>
            {!! Form::text('skype', $user->skype, array('class' => 'form-control')) !!}
            <p>Время работы</p>
            {!! Form::text('worktime', $user->worktime, array('class' => 'form-control')) !!}
            <p>Год рождения</p>
            {!! Form::text('bornyear', $user->bornyear, array('class' => 'form-control')) !!}
            <p>Фотография</p>
            @if($user->foto != '')
            <div>
                <img src="{{asset('/upload/users/'.$user->foto)}}" alt="" class="img-responsive">
            </div>
            @endif
            {!! Form::file('foto', '', array('class' => 'form-control')) !!}
            <button type="submit" class="btn btn-info" style="margin-top:20px;">Сохранить</button>
            {!! Form::close() !!}
        </div>
    </section>

@endsection