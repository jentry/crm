@extends('layout.layout')

@section('content')
    <section class="main col-xs-10">
        @if(Auth::user()->is('admin'))
            <h1>Добавить совет</h1>
            {!! Form::open(array('url' => '/faq/store', 'method' => 'POST', 'files' => true)) !!}
            <p>Название</p>
            {!! Form::text('title', '', array('class' => 'form-control')) !!}
            <p>Текст совета</p>
            {!! Form::textarea('text', '', array('class' => 'form-control textarea')) !!}
            <p>Аудио-файл</p>
            {!! Form::file('audio', '', array('class' => 'form-control')) !!}
            <button type="submit" class="btn btn-info" style="argin-top:25px;">Сохранить</button>
            {!! Form::close() !!}
        @endif
    </section>
@endsection