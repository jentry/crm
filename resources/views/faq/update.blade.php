@extends('layout.layout')

@section('content')
    <section class="main col-xs-10">
        @if(Auth::user()->is('admin'))
            <h1>Редактировать совет</h1>
            {!! Form::open(array('url' => '/faq/update', 'method' => 'PUT', 'files' => true)) !!}
            {!! Form::hidden('id', $faq->id) !!}
            <p>Название</p>
            {!! Form::text('title', $faq->title, array('class' => 'form-control')) !!}
            <p>Текст совета</p>
            {!! Form::textarea('text', $faq->text, array('class' => 'form-control textarea')) !!}
            <p>Аудио-файл</p>
            <p>Файл: {{ $faq->audio }}</p>
            {!! Form::file('audio', '', array('class' => 'form-control')) !!}
            <button type="submit" class="btn btn-info" style="argin-top:25px;">Редактировать</button>
            {!! Form::close() !!}
        @endif
    </section>
@endsection