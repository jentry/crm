@extends('layout.layout')

@section('content')
    <section class="main col-xs-10">
        @if (session('mess'))
            <div class="alert alert-success">
                {{ session('mess') }}
            </div>
        @endif
        @if(Auth::user()->is('admin'))
            <h1>Список советов FAQ</h1>
            <ul class="list-unstyled">
            @foreach($all as $one)
              <li>
                  <a href="{{url('/faq/'.$one->id)}}">{{$one->title}}</a>
              </li>
            @endforeach
            </ul>
            <p><a href="{{url('faq/create')}}">Добавить совет</a></p>
        @endif
    </section>
@endsection