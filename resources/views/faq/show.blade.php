@extends('layout.layout')

@section('content')
    <section class="main col-xs-10">
        <h1>{{$faq->title}}</h1>
        {!! $faq->text !!}
        <p>Аудио файл: {{$faq->audio}}</p>
        <p>Редактировать: <a href="{{url('/faq/edit/'.$faq->id)}}">Редактировать</a></p>
        {!! Form::open(['url' => '/faq/delete', 'method' => 'DELETE']) !!}
        {!! Form::hidden('id', $faq->id) !!}
        <button type="submit" class="btn btn-danger">Удалить совет</button>
        {!! Form::close() !!}
    </section>
@endsection