@extends('layout.layout')

@section('content')
    <section class="main" style="padding: 0 15px 75px;">
        <h1>Результаты поиска по запросу: {{$query}}</h1>
        @if(count($res['users']))
            <p>Пользователи:</p>
            <ul class="list-unstyled">
                @foreach($res['users'] as $user)
                    <li><a href="/manager/public/{{$user->id}}">{{$user->name}}</a></li>
                @endforeach
            </ul>
        @endif
        @if(count($res['tasks']))
            <p>Задачи:</p>
            <ul class="list-unstyled">
                @foreach($res['tasks'] as $task)
                    <li><a href="/task/{{$task->id}}">{{$task->title}}</a></li>
                @endforeach
            </ul>
        @endif
    </section>
@endsection