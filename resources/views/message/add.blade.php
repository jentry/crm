@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">

        <nav>
            <p>Диалоги:</p>
            <ul class="list-unstyled">
                @foreach($dialogs as $dialog)
                    <li><a href="{{url('/message/single/'.$dialog->id)}}">{{$dialog->name}}</a></li>
                @endforeach
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1>Написать сообщения</h1>
        {!! Form::open(['url' => '/message/add', 'method' => 'POST']) !!}
        <div class="whom">
            <p>Кому: <a href="#" class="choose_user">Выбрать</a></p>
            <div class="chooser">
                <ul class="list-unstyled">
                    @foreach($users as $user)
                        <li class="group">
                            <div class="foto">
                                <img src="/upload/users/{{$user->foto}}" alt="" class="img-responsive">
                            </div>
                            @if($user->id == $id)
                                {!! Form::radio('target_id', $user->id, true, ['id' => 'whom'.$user->id]) !!}
                            @else
                                {!! Form::radio('target_id', $user->id, false, ['id' => 'whom'.$user->id]) !!}
                            @endif
                            <label for="whom{{$user->id}}">{{$user->name}}</label>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        {!! Form::textarea('message', '', ['class' => 'form-control']) !!}
        <button type="submit" class="btn btn-info" style="margin-top: 20px;">Отправить</button>
        {!! Form::close() !!}
    </section>
@endsection