@extends('layout.layout')

@section('content')
    <aside class="col-xs-2">
        <nav>
            <p style="padding-left: 10px;">Диалоги:</p>
            <ul class="list-unstyled">
                @foreach($dialogs as $dialogss)
                    <li><a href="{{url('/message/single/'.$dialogss->id)}}">{{$dialogss->name}}</a></li>
                @endforeach
            </ul>
        </nav>
    </aside>
    <section class="main col-xs-10">
        <h1 style="margin-bottom: 25px;">Ваша переписка с {{$uUser->name}}</h1>
        @foreach($dialog as $mess)
            <div class="message group @if($mess->author->id == $id) another @endif">
                <div class="col-xs-10">
                    <p>{{$mess->message}}</p>
                    <p class="author"><small>{{$mess->author->name}}</small></p>
                </div>
                <div class="col-xs-2 time">
                    {{ date('d m Y H:i', strtotime($mess->created_at)) }}
                    <br>
                    <a href="#" data-id="{{$mess->id}}" class="delete_mess">Удалить</a>
                </div>
            </div>
        @endforeach

        <p>Написать сообщение:</p>
        {!! Form::open(['url' => '/messages/add', 'method' => 'POST']) !!}
        {!! Form::hidden('target_id', $uUser->id) !!}
        {!! Form::textarea('message', '', ['class' => 'form-control']) !!}
        <button type="submit" class="btn btn-info" style="margin-top: 20px;">Отправить</button>
        {!! Form::close() !!}
    </section>
@endsection