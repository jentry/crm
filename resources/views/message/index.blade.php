@extends('layout.layout')

@section('content')
    <section class="main col-xs-12">
        <h1 class="pull-left">Диалоги:</h1>
        <a href="{{url('/message/add')}}" class="add_new_task pull-right" style="width:180px;">Написать сообщение</a>
        <div class="clearfix"></div>
        @foreach($dialogs as $dialog)
            <div class="single_dialog">
                <div class="foto">
                    <img src="{{asset('/upload/users/'.$dialog->foto)}}" alt="" class="img-responsive">
                </div>
                <p class="name"><a href="{{url('/message/single/'.$dialog->id)}}">{{$dialog->name}}</a></p>
                <p><small>{{$dialog->otdel}}</small></p>
            </div>
        @endforeach
    </section>
@endsection