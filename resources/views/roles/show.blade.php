@extends('layout/layout')

@section('content')
    <section id="roles" style="padding: 0 15px;">
        <h1>{{$role->name}}</h1>
        <p>{{$role->description}}</p>
        {!! Form::open(['url' => 'managersrole/delete', 'method' => 'DELETE']) !!}
        {!! Form::hidden('id', $role->id) !!}
        <button type="submit" class="btn btn-primary">Удалить роль</button>
        {!! Form::close() !!}
    </section>
@endsection