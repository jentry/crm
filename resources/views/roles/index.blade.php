@extends('layout/layout')

@section('content')
    <section id="roles" style="padding: 0 15px;">
        <h1>Все роли</h1>
        <ul class="list-unstyled">
            @foreach($all as $one)
                <li>
                    <p><a href="{{url('/managersrole/'.$one->id)}}">{{$one->name}}</a></p>
                </li>
            @endforeach
        </ul>
        <p><a href="{{url('/managersrole/add')}}">Добавить роль</a></p>
    </section>
@endsection