@extends('layout/layout')

@section('content')
    <section id="roles" style="padding: 0 15px;">
        <h1>Добавить роль</h1>
        {!! Form::open(['url' => '/managersrole/add', 'method' => 'POST']) !!}
        <p>Название роли</p>
        {!! Form::text('name', '', ['class' => 'form-control']) !!}
        <p>Slug роли</p>
        {!! Form::text('slug', '', ['class' => 'form-control']) !!}
        <p>Описание ролb</p>
        {!! Form::text('description', '', ['class' => 'form-control']) !!}
        <p></p>
        <button type="submit" class="btn btn-info">Создать</button>
        {!! Form::close() !!}
    </section>
@endsection