<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daysalary extends Model
{

    protected $table = 'daysalaries';

    protected $fillable = ['user_id', 'summ', 'date'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
