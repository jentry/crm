<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $fillable = ['title', 'description', 'deadline', 'fire', 'user_id', 'author_id', 'favorite', 'status', 'percent', 'parent_id', 'finished'];

    public function parent()
    {
        return $this->belongsTo('App\Task', 'parent_id');
    }

    public function kids()
    {
        return $this->hasMany('App\Task', 'parent_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Taskcomment', 'task_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
}
