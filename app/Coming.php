<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coming extends Model
{
    protected $fillable = ['user_id', 'intime', 'outtime', 'day'];
}
