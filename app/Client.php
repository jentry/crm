<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'deadline', 'comment', 'islike', 'blacklist', 'status', 'user_id'];

    public function comments()
    {
        return $this->hasMany('App\Comment', 'client_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
