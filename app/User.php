<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Authenticatable implements HasRoleAndPermissionContract
{

    use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'otdel', 'phone', 'skype', 'worktime', 'bornyear', 'foto', 'salary', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('Bican\Roles\Models\Role', 'role_user', 'user_id', 'role_id');
    }

    public function premias()
    {
        return $this->hasMany('App\Premia', 'user_id');
    }

    public function avanses()
    {
        return $this->hasMany('App\Avans', 'user_id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification', 'user_id');
    }

    public function daysalaries()
    {
        return $this->hasMany('App\Daysalary', 'user_id');
    }
}
