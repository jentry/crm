<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Coming;

class UpdateUserMetaData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedIn  $event
     * @return void
     */
    public function handle(UserLoggedIn $event)
    {
        Coming::create([
            'user_id' => $event->userId,
            'intime' => (new \DateTime())->format('H:i:s'),
            'outtime' => null,
            'day' => (new \DateTime())->format('Y-m-d')
        ]);
    }
}
