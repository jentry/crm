<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\Coming;
use Session;


class UserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function onUserLogout(Logout $event)
    {
        $value = app('request')->session()->get('coming');
        $value = Session::get('coming');
        $coming = Coming::find($value);
        $coming->outtime = (new \DateTime())->format('H:i');
        $coming->save();

    }

    public function onUserLogin(Login $event)
    {
        $date = (new \DateTime());
        $date->add(new \DateInterval('PT1H'));
        $coming = Coming::create([
            'user_id' => Auth::user()->id,
            'intime' => (new \DateTime())->format('H:i'),
            'outtime' => $date->format('H:i'),
            'day' => (new \DateTime())->format('Y-m-d')
        ]);


        Session::put('coming', $coming->id);
    }
}
