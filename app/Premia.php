<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Premia extends Model
{
    protected $fillable = ['user_id', 'day', 'month', 'year', 'summ'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
