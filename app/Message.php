<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['author_id', 'target_id', 'message', 'readed', 'deleted'];

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function target()
    {
        return $this->belongsTo('App\User', 'target_id');
    }
}
