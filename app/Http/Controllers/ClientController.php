<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Bican\Roles\Models\Role;
use Goutte\Client as Goutte;
use App\Faq;
use App\Client;
use App\Comment;
use Auth;
use DB;

class ClientController extends Controller
{
    public function index()
    {
        $faqs = Faq::all();
        $clients = Client::where([['islike', 1], ['status', '!=', 'В проект'], ['user_id', Auth::user()->id]])->get();

        $zaprosFile = DB::table('options')->where('id', 1)->first();


        return view('client.index', ['faqs' => $faqs, 'clients' => $clients]);
    }


    public function search(Request $request)
    {

        $zaprosFile = DB::table('options')->where('id', 1)->first();
        //$file_name = public_path().'/upload/files/'.$zaprosFile->file;
        $file_name = "/home/t/tigranla/db.es-stone.ru/public_html/upload/files/".$zaprosFile->file;
        $array = explode("\n", file_get_contents("$file_name"));
        $max = count($array) - 1;
        $min = 0;
        $val = rand($min, $max);
        $text = $array[$val];

        $text_query = str_replace(' ', '+', $text);
        $query = "https://yandex.ru/search/xml?user=shiftcrm&key=03.386017525:a106a4c2201cac1f1c0206075f77c2e2&query=".$text_query."&l10n=ru&sortby=tm.order%3Dascending&filter=moderate&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D100.docs-in-group%3D1";
        $res = file_get_contents($query);
        $res = $xmlstr = <<<XML
$res
XML;

		//$xml = simplexml_load_string($res, 'SimpleXMLElement');
        $xml = new \SimpleXMLElement($res);
        //return response()->json(['xml' => $xml]);
        //$xml = simplexml_load_file();
        $results = $xml->response->results->grouping->group;
        //return response()->json(['xml' => $results]);
        //return $results;
		$rand = rand(3,99);
        $domain = $results[$rand]->doc->domain;
        $none = Client::where('blacklist', '1')->orWhere('islike', '0
        ')->lists('name')->toArray();
        //проверка на вхождение в blacklist или отрицательные

        /*$in_none = in_array($domain, $none);
        while(!$in_none){
            $rand = rand(3,99);
            $domain = $results[$rand]->doc->domain;
            $in_none = in_array($domain, $none);
        }*/

        //

        $data = [];
        $datas = [];
        $top1 = $results[0]->doc->domain;
        $top2 = $results[1]->doc->domain;
        $top3 = $results[2]->doc->domain;
        $domains = [
            0 => $domain,
            1 => $top1,
            2 => $top2,
            3 => $top3
        ];
        $datas[0] = $domains;

        for($i = 0; $i < count($domains); $i++){
            $site_url = $domains[$i];
            $url ="http://pr-cy.ru/a/".$site_url;
            $client = new Goutte();
            $crawler = $client->request(
                'GET',
                $url
            );
            $text = $crawler->filter('#box-basik a.bold.black')->extract(array('_text'));
            $text1 = $crawler->filter('#box-basik a.bold.green-normal')->extract(array('_text'));
            $text2 = $crawler->filter('#box-basik div.pull-right b')->extract(array('_text'));

            if(count($text1)) {
                $dataYandex[0] = $text[0];
                $dataYandex[1] = $text[1];
                $dataYandex[2] = $text1[0];
                $dataGoogle[0] = $text2[0];
                $dataGoogle[1] = $text[3];
                $dataGoogle[2] = $text1;
            }
            else{
                $text1 = $crawler->filter('#box-basik a.bold.red-light')->extract(array('_text'));
                if(isset($text[0])){
                    $dataYandex[0] = $text[0];
                }
                else{
                    $dataYandex[0] = '';
                }
                if(isset($text[1])){
                    $dataYandex[2] = $text[1];
                }
                else{
                    $dataYandex[2] = '';
                }
                if(isset($text1[0])){
                    $dataYandex[1] = $text1[0];
                }
                else{
                    $dataYandex[1] = '';
                }
                if(isset($text2[0])){
                    $dataGoogle[0] = $text2[0];
                }
                else{
                    $dataGoogle[0] = '';
                }
                if(isset($text[3])){
                    $dataGoogle[2] = $text[3];
                }
                else{
                    $dataGoogle[2] = '';
                }

                $dataGoogle[1] = $text1;
            }
            $data[$i][0] = $dataYandex;
            $data[$i][1] = $dataGoogle;
        }
        $datas[1] = $data;
        $datas['rand'] = $rand;
        $datas['zapros'] = $array[$val];
        return $datas;


    }

    public function randomsite(Request $request)
    {
        $site = $request->site;
        $site_url = str_replace('http://','', $site);
        $url ="http://pr-cy.ru/a/".$site_url;
        $client = new Goutte();
        $crawler = $client->request(
            'GET',
            $url
        );
        $text = $crawler->filter('#box-basik a.bold.black')->extract(array('_text'));
        $text1 = $crawler->filter('#box-basik a.bold.green-normal')->extract(array('_text'));
        $text2 = $crawler->filter('#box-basik div.pull-right b')->extract(array('_text'));

        if(!count($text1)) {
            $text1 = $crawler->filter('#box-basik a.bold.red-light')->extract(array('_text'));
            $dataYandex[0] = $text[0];
            $dataYandex[2] = $text[1];
            $dataYandex[1] = $text1[0];
            $dataGoogle[0] = $text2[0];
            $dataGoogle[2] = $text[3];
//            $dataGoogle[1] = $text1[1];
            $dataGoogle[1] = $text1[0];
        }
        else{
            $dataYandex[0] = $text[0];
            $dataYandex[1] = $text[1];
            $dataYandex[2] = $text1[0];
            $dataGoogle[0] = $text2[0];
            //$dataGoogle[1] = $text[3];
            $dataGoogle[1] = $text1[0];
            //$dataGoogle[2] = $text1[1];
            $dataGoogle[2] = $text[3];
        }




        $data[0] = array();//random site
        $data[0][0] = $dataYandex;
        $data[0][1] = $dataGoogle;
        return $data;
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'site' => 'required'
        ]);

        $client = Client::create([
            'name' => $request->site,
            'deadline' => $request->time,
            'status' => $request->type,
            'comment' => '',
            'islike' => $request->isLike,
            'blacklist' => $request->blacklist,
            'user_id' => Auth::user()->id
        ]);

        if($request->comment != ''){
            Comment::create([
                'client_id' => $client->id,
                'user_id' => Auth::user()->id,
                'text' => $request->comment
            ]);
        }


        return $client->id;

    }

    public function sort(Request $request)
    {
        if((int)$request->blacklist == 1){
            $clients = Client::where([['blacklist', (int)$request->blacklist], ['status', '!=', 'В проект'], ['user_id', Auth::user()->id]])->get();
        }
        elseif($request->blacklist == 'project'){
            $clients = Client::where([['status', 'В проект'], ['user_id', Auth::user()->id]])->get();
        }
        else {
            $clients = Client::where([['islike', (int)$request->isLike], ['blacklist', (int)$request->blacklist], ['status', '!=', 'В проект'], ['user_id', Auth::user()->id]])->get();
        }
        //return response()->json($clients);
        return view('ajax.tr', ['clients' => $clients]);

    }

    public function edit(Request $request)
    {
        $client = Client::find($request->id);
        return view('ajax.edit', ['client' => $client]);
    }

    public function update(Request $request)
    {
        $client = Client::find($request->id);
        $client->name = $request->site;
        $client->deadline = $request->time;
        //$client->comment = $request->comment;
        $client->status = $request->type;
        $client->save();

        if($request->comment != ''){
            Comment::create([
                'client_id' => $client->id,
                'user_id' => Auth::user()->id,
                'text' => $request->comment
            ]);
        }

        return response()->json(['client' => $client]);
    }

    public function comments(Request $request)
    {
        $client = Client::find($request->id);
        $comments = Comment::where('client_id', $request->id)->get();
        return view('ajax.comments', ['comments' => $comments, 'client' => $client]);
    }

    public function commentadd(Request $request)
    {
        Comment::create([
            'text' => $request->text,
            'client_id' => $request->client,
            'user_id' => Auth::user()->id
        ]);

        $client = Client::find($request->client);
        $comments = Comment::where('client_id', $request->client)->get();
        return view('ajax.comments', ['comments' => $comments, 'client' => $client]);
    }

    public function edittime(Request $request)
    {
        $client = Client::find($request->id);
        return view('ajax.deadline', ['client' => $client]);
    }

    public function updatetime(Request $request)
    {
        $client = Client::find($request->id);
        $client->deadline = $request->time;
        $client->save();
        return response()->json(['client' => $request->time]);
        //return view('ajax.deadline', ['client' => $client]);
    }

    public function editstatus(Request $request)
    {
        $client = Client::find($request->id);
        return view('ajax.status', ['client' => $client]);
//        return response()->json(['client' => $client]);
    }

    public function updatestatus(Request $request)
    {
        $client = Client::find($request->id);
        $client->status = $request->status;
        $client->save();
    }

    public function files(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        if ($request->hasFile('file')) {
            $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/files/";
            //$destinationPath = public_path().'/upload/files/';
            $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
            $filename = rand(11111,99999).'.'.$extension; // renameing image
            $request->file('file')->move($destinationPath, $filename);

            $st = DB::table('options')->where('id', 1)->update(['file' => $filename, 'user_id' => Auth::user()->id]);
        }
        return redirect('newclient');

    }
}
