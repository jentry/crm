<?php

namespace App\Http\Controllers;

use Faker\Provider\zh_TW\DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\User;
use App\Daysalary;

class DaysalaryController extends Controller
{
    public function index()
    {
        $users = User::all();
        $users_count = $users->count();
        return view('daysalary.index', ['count' => $users_count, 'users' => $users]);
    }

    public function add($id)
    {
        $user = User::find($id);
        $users = User::all();
        $users_count = $users->count();
        return view('daysalary.add', ['user' => $user, 'count' => $users_count]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required',
            'summ' => 'required'
        ]);

        $datas = explode(' ', $request->data);
        Daysalary::create([
            'user_id' => $request->user,
            'summ' => $request->summ,
            'date' => (new \DateTime($datas[0].'-'.$datas[1].'-'.$datas[2]))
        ]);
        return redirect('/daysalary/add/'.$request->user)->with('message', '�������� ���������');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'summ' => 'required'
        ]);

        $salary = Daysalary::find($request->id);
        $salary->summ = $request->summ;
        $salary->save();
        return redirect('/daysalary/add/'.$request->user)->with('message', '�������� ���������');
    }
}
