<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Notification;
use Auth;
use DB;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
            DB::table('notifications')
                ->where([
                    ['user_id', Auth::user()->id],
                    ['type', $request->type]
                ])
                ->update(['readed' => true]);
    }
}
