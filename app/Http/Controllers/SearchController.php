<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Task;


class SearchController extends Controller
{
    public function index(Request $request)
    {
        $query = $request->search;
        if($query != ''){
            $usersQuery = User::where("name", "LIKE", "%$query%")->get();
            $tasksQuery = Task::where("title", "LIKE", "%$query%")->get();
            $res['users'] = $usersQuery;
            $res['tasks'] = $tasksQuery;
        }
        else{
            $res = [];
        }
        return view('search.index', ['res' => $res, 'query' => $query]);
    }
}
