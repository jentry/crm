<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Task;
use Auth;
use Bican\Roles\Models\Role;
use DB;
use App\Taskcomment;
use App\Notification;

class TaskController extends Controller
{

    public function index()
    {

        $me = Auth::user()->id;

        $in = Task::where('user_id', $me)->get();
        $out = Task::where('author_id', $me)->get();
        //$all = (int)$in->count() + (int)$out->count();
        $all1 = Task::where('author_id', $me);
        $all = Task::where('user_id', $me)->union($all1)->get();
        $all = $all->unique('id');
        $allCount = count($all->values()->all());
        $finished = Task::where('user_id', $me)->union($all1)->get();
        $finished = $finished->where('finished', 1)->unique('id')->values()->all();
        $pretasks = Task::where([['author_id', $me], ['parent_id', '0'], ['finished', 0]]);
        $tasks = Task::where([['user_id', $me], ['parent_id', '0'], ['finished', 0]])->union($pretasks)->get();
        $tasks = $tasks->unique('id')->values()->all();

        return view('task.index', ['tasks' => $tasks, 'all' => $allCount, 'inCount' => (int)$in->count(), 'outCount' => (int)$out->count(), 'finCount' => count($finished)]);
    }

    public function add($userid = null)
    {
        $me = Auth::user()->id;

        $in = Task::where('user_id', $me)->get();
        $out = Task::where('author_id', $me)->get();
        $all = (int)$in->count() + (int)$out->count();
        $users = User::all();
        $roles = Role::all();

        $tasks = Task::where('finished', '0')->lists('title', 'id');
        return view('task.create', ['userid' => $userid, 'all' => $all, 'users' => $users, 'userid' => $userid, 'roles' => $roles, 'tasks' => $tasks]);
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'user' => 'required',
            'deadline' => 'required'
        ]);

        $d = \Datetime::createFromFormat('d m Y', $request->deadline);
        $fire = $request->fire;
        if($fire == ''){
            $fire = 0;
        }

        $task = Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'deadline' => $d->format('Y-m-d H:i:s'),
            'parent_id' => $request->parent_id,
            'fire' => $fire,
            'user_id' => $request->user,
            'author_id' => Auth::user()->id,
            'favorite' => false,
            'status' => 'Назначена',
            'percent' => 0,
            'finished' => false
        ]);

        Notification::create([
            'user_id' => $request->user,
            'message' => 'Вас назначено исполнителем на задачу <a href="/task/'.$task->id.'">'.$task->title.'</a>',
            'readed' => false,
            'type' => 2
        ]);

        if(!empty($request->suser)){
            foreach($request->suser as $suser){
                DB::table('task_susers')->insert([
                    'task_id' => $task->id,
                    'user_id' => $suser
                ]);
                Notification::create([
                    'user_id' => $suser,
                    'message' => 'Вас назначено со-исполнителем на задачу <a href="/task/'.$task->id.'">'.$task->title.'</a>',
                    'readed' => false,
                    'type' => 2
                ]);
            }
        }

        if(!empty($request->auditor)){
            foreach($request->auditor as $auditor){
                DB::table('tasks_auditors')->insert([
                    'task_id' => $task->id,
                    'user_id' => $auditor
                ]);

                Notification::create([
                    'user_id' => $auditor,
                    'message' => 'Вас назначено аудитором на задачу <a href="/task/'.$task->id.'">'.$task->title.'</a>',
                    'readed' => false,
                    'type' => 1
                ]);
            }
        }
        $files = $request->files;
        if(count($request->files)){
            foreach( $files as $file1){
                foreach($file1 as $file) {
                    if($file):
                    //$destinationPath = public_path() . '/upload/tasks/'; // upload path
                    $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/tasks/";
					$extension = $file->getClientOriginalExtension();
                    $filename = rand(11111, 99999) . '.' . $extension; // renameing image
                    $file->move($destinationPath, $filename);
                    DB::table('tasks_files')->insert([
                        'task_id' => $task->id,
                        'file_name' => $filename
                    ]);
                    endif;
                }
            }
        }

        return redirect('/tasks');
    }

    public function show($id, Request $request)
    {
        $task = Task::find($id);
        $files = DB::table('tasks_files')->where('task_id', $id)->get();

        $susersId = DB::table('task_susers')->where('task_id', $id)->lists('user_id');
        $susers = User::whereIn('id', $susersId)->get();

        $auditorsId = DB::table('tasks_auditors')->where('task_id', $id)->lists('user_id');
        $auditors = User::whereIn('id', $auditorsId)->get();
        return view('task.show', ['task' => $task, 'files' => $files, 'susers' => $susers, 'auditors' => $auditors]);
    }

    public function edit($id)
    {
        $task = Task::find($id);
        $me = Auth::user()->id;

        $in = Task::where('user_id', $me)->get();
        $out = Task::where('author_id', $me)->get();
        $all = (int)$in->count() + (int)$out->count();
        $users = User::all();
        $roles = Role::all();

        $tasks = Task::lists('title', 'id')->all();

        $susersId = DB::table('task_susers')->where('task_id', $id)->lists('user_id');
        $auditorsId = DB::table('tasks_auditors')->where('task_id', $id)->lists('user_id');

        return view('task.edit', ['task' => $task, 'all' => $all, 'tasks' => $tasks, 'users' => $users, 'roles' => $roles, 'susersId' => $susersId, 'auditorsId' => $auditorsId]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'user' => 'required',
            'deadline' => 'required'
        ]);

        $d = \Datetime::createFromFormat('d m Y', $request->deadline);

        $task = Task::find($request->id);
        $task->title = $request->title;
        $task->description = $request->description;
        $task->deadline = $d;
        $task->parent_id = $request->parent_id;
        $task->fire = $request->fire;
        $task->user_id = $request->user;
        $task->save();

        Notification::create([
            'user_id' => $request->user,
            'message' => 'Изменились условия задачи <a href="/task/'.$task->id.'">'.$task->title.'</a>, в которой вы исполнитель',
            'readed' => false,
            'type' => 2
        ]);

        if(!empty($request->suser)){
            DB::table('task_susers')->where('task_id', $request->id)->delete();
            foreach($request->suser as $suser){
                DB::table('task_susers')->insert([
                    'task_id' => $task->id,
                    'user_id' => $suser
                ]);
                Notification::create([
                    'user_id' => $suser,
                    'message' => 'Изменились условия задачи <a href="/task/'.$task->id.'">'.$task->title.'</a>, в которой вы со-исполнитель',
                    'readed' => false,
                    'type' => 2
                ]);
            }
        }

        if(!empty($request->auditor)){
            DB::table('tasks_auditors')->where('task_id', $request->id)->delete();
            foreach($request->auditor as $auditor){
                DB::table('tasks_auditors')->insert([
                    'task_id' => $task->id,
                    'user_id' => $auditor
                ]);
                Notification::create([
                    'user_id' => $auditor,
                    'message' => 'Изменились условия задачи <a href="/task/'.$task->id.'">'.$task->title.'</a>, в которой вы аудитор',
                    'readed' => false,
                    'type' => 2
                ]);
            }
        }
        $files = $request->files;
        if(!empty($request->files)){
            foreach( $files as $file1){
                    foreach ($file1 as $file) {
                        if(!empty($file)) {
                            //$destinationPath = public_path() . '/upload/tasks/'; // upload path
                            $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/tasks/";
							$extension = $file->getClientOriginalExtension();
                            $filename = rand(11111, 99999) . '.' . $extension; // renameing image
                            $file->move($destinationPath, $filename);
                            DB::table('tasks_files')->insert([
                                'task_id' => $task->id,
                                'file_name' => $filename
                            ]);
                        }
                    }

            }
        }

        return redirect('/task/'.$request->id);
    }

    public function changefav(Request $request)
    {
        $task = Task::find($request->id);
        $task->favorite = $request->type;
        $task->save();
    }

    public function sub(Request $request)
    {
        $task = Task::find($request->id);
        return view('ajax.subtask', ['task' => $task]);
    }

    public function addcomment(Request $request)
    {
        $this->validate($request, [
           'comment' => 'required'
        ]);

        $comment = Taskcomment::create([
            'text' => $request->comment,
            'user_id' => Auth::user()->id,
            'task_id' => $request->id
        ]);

        $files = $request->files;
        if(!empty($request->files)){
            foreach( $files as $file1){
                foreach($file1 as $file) {
                    //$destinationPath = public_path() . '/upload/taskcomments/'; // upload path
                    $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/taskcomments/";
					$extension = $file->getClientOriginalExtension();
                    $filename = rand(11111, 99999) . '.' . $extension; // renameing image
                    $file->move($destinationPath, $filename);
                    DB::table('taskcomments_files')->insert([
                        'comment_id' => $comment->id,
                        'file' => $filename
                    ]);
                }
            }
        }

        $task = Task::find($request->id);
        //notif for auditors
        $auditors = DB::table('tasks_auditors')
            ->select('user_id')
            ->where('task_id', $task->id)
            ->get();

        if(count($auditors)) {
            foreach ($auditors as $auditor) {
                Notification::create([
                    'user_id' => $auditor->user_id,
                    'message' => 'К задаче <a href="/task/' . $task->id . '">' . $task->title . '</a> добавлен комментарий',
                    'readed' => false,
                    'type' => 1
                ]);
            }
        }
        //notif for author
        Notification::create([
            'user_id' => $task->author_id,
            'message' => 'К задаче <a href="/task/' . $task->id . '">' . $task->title . '</a> добавлен комментарий',
            'readed' => false,
            'type' => 1
        ]);
        //notif for user
        Notification::create([
            'user_id' => $task->user_id,
            'message' => 'К задаче <a href="/task/' . $task->id . '">' . $task->title . '</a> добавлен комментарий',
            'readed' => false,
            'type' => 1
        ]);



        return redirect('/task/'.$request->id);
    }

    public function getcomment(Request $request)
    {
        $comment = Taskcomment::find($request->id);
        return view('ajax.comment', ['comment' => $comment]);
    }

    public function editcomment(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required'
        ]);

        $comment = Taskcomment::find($request->id);
        $comment->text = $request->comment;
        $comment->save();

        $files = $request->files;
        if(!empty($request->files)){
            foreach( $files as $file1){
                foreach($file1 as $file) {
                    //$destinationPath = public_path() . '/upload/taskcomments/'; // upload path
                    $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/taskcomments/";
					$extension = $file->getClientOriginalExtension();
                    $filename = rand(11111, 99999) . '.' . $extension; // renameing image
                    $file->move($destinationPath, $filename);
                    DB::table('taskcomments_files')->insert([
                        'comment_id' => $comment->id,
                        'file' => $filename
                    ]);
                }
            }
        }
        return redirect('/task/'.$request->id);
    }

    public function susersget(Request $request)
    {
        $users = User::all();
        $susers = DB::table('task_susers')->where('task_id', $request->id)->lists('user_id');
        $roles = Role::all();
        return view('ajax.susers', ['users' => $users, 'susers' => $susers, 'roles' => $roles, 'id' => $request->id]);
    }

    public function suserupdate(Request $request)
    {
        DB::table('task_susers')->where('task_id', $request->id)->delete();
        foreach($request->suser as $user){
            DB::table('task_susers')->insert([
                'task_id' => $request->id,
                'user_id' => $user
            ]);
        }

        return redirect('/task/'.$request->id);
    }

    public function auditorsget(Request $request)
    {
        $users = User::all();
        $auditors = DB::table('tasks_auditors')->where('task_id', $request->id)->lists('user_id');
        $roles = Role::all();
        return view('ajax.auditors', ['users' => $users, 'auditors' => $auditors, 'roles' => $roles, 'id' => $request->id]);
    }

    public function auditorsupdate(Request $request)
    {
        DB::table('tasks_auditors')->where('task_id', $request->id)->delete();
        foreach($request->auditor as $user){
            DB::table('tasks_auditors')->insert([
                'task_id' => $request->id,
                'user_id' => $user
            ]);
        }

        return redirect('/task/'.$request->id);
    }

    public function percent(Request $request)
    {
        $task = Task::find($request->id);
        return response()->json(['percent' => $task->percent]);
    }

    public function percentadd(Request $request)
    {
        $task = Task::find($request->id);
        $task->percent = $request->percents;
        $task->save();
        return response()->json(['percent' => $task->percents]);
    }

    public function status(Request $request)
    {
        $task = Task::find($request->id);

        return response()->json(['status' => $task->status]);
    }

    public function statusadd(Request $request)
    {
        $task = Task::find($request->id);
        $task->status = $request->status;
        $task->save();

        Notification::create([
            'user_id' => $task->user_id,
            'message' => 'Изменился статус задачи <a href="/task/'.$task->id.'">'.$task->title.'</a>, в которой вы исполнитель',
            'readed' => false,
            'type' => 2
        ]);

        Notification::create([
            'user_id' => $task->author_id,
            'message' => 'Изменился статус задачи <a href="/task/'.$task->id.'">'.$task->title.'</a>, в которой вы постановщик',
            'readed' => false,
            'type' => 2
        ]);

        return response()->json(['status' => $task->status]);
    }

    public function finish(Request $request)
    {
        $task = Task::find($request->id);
        $task->finished = $request->state;
        $task->save();

        //notif for auditors
        $auditors = DB::table('tasks_auditors')
            ->select('user_id')
            ->where('task_id', $task->id)
            ->get();

        if(count($auditors)) {
            foreach ($auditors as $auditor) {
                Notification::create([
                    'user_id' => $auditor->user_id,
                    'message' => 'Задача <a href="/task/' . $task->id . '">' . $task->title . '</a> закрыта',
                    'readed' => false,
                    'type' => 1
                ]);
            }
        }
        Notification::create([
            'user_id' => $task->user_id,
            'message' => 'Задача <a href="/task/' . $task->id . '">' . $task->title . '</a> закрыта',
            'readed' => false,
            'type' => 1
        ]);
        Notification::create([
            'user_id' => $task->author_id,
            'message' => 'Задача <a href="/task/' . $task->id . '">' . $task->title . '</a> закрыта',
            'readed' => false,
            'type' => 1
        ]);

        return response()->json(['task' => $task]);
    }

    public function sort(Request $request)
    {
        $me = Auth::user()->id;
        if ($request->type == 'finished') {
            $all1 = Task::where('author_id', $me);
            $finished = Task::where('user_id', $me)->union($all1)->get();
            $tasks = $finished->where('finished', 1)->unique('id')->values()->all();
        }
        elseif($request->type == 'user'){
            $tasks = Task::where('user_id', $me)->get();
        }
        elseif($request->type == 'author'){
            $tasks = Task::where('author_id', $me)->get();
        }
        //return $tasks;
        return view('ajax.tasks', ['tasks' => $tasks]);
    }

}
