<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Bican\Roles\Models\Role;

class ManagerController extends Controller
{

    public function index()
    {
        $users = User::all();
        $users_count = $users->count();
        return view('manager.index', ['users' => $users, 'count' => $users_count]);
    }

    public function show()
    {
        $current_id = Auth::user()->id;
        $user = User::find($current_id);
        $users_count = User::all()->count();
        return view('manager.show', ['user' => $user, 'count' => $users_count]);
    }

    public function edit(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:255',
            'otdel' => 'required',
            'email' => 'required'
        ]);

        if ($request->hasFile('foto')) {
            //$destinationPath = public_path().'/upload/users/'; // upload path
            $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/users/";
            $extension = $request->file('foto')->getClientOriginalExtension(); // getting image extension
            $filename = rand(11111,99999).'.'.$extension; // renameing image
            $request->file('foto')->move($destinationPath, $filename);
        }
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->otdel = $request->otdel;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->skype = $request->skype;
        $user->worktime = $request->worktime;
        $user->bornyear = $request->bornyear;
        if($request->password != ''){
            $user->password = bcrypt($request->password);
        }
        if ($request->hasFile('foto')){
            $user->foto = $filename;
        }
        $user->save();
        return redirect('/managers/')->with('mess', 'Ваш профиль обновлен');

    }

    public function manager($id)
    {
        $user = User::find($id);
        $users_count = User::all()->count();
        return view('manager.public', ['user' => $user, 'count' => $users_count]);
    }

    public function helping()
    {
        $users_count = User::all()->count();
        return view('manager.helping', ['count' => $users_count]);
    }

    public function add()
    {
        $users_count = User::all()->count();
        $roles = Role::all();
        return view('manager.add', ['count' => $users_count, 'roles' => $roles]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6',
        ]);

        $role = Role::find($request->role);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'otdel' => $request->otdel,
            'phone' => $request->phone,
            'skype' => $request->phone,
            'worktime' => $request->worktime,
            'bornyear' => $request->bornyear,
            'salary' => $request->salary,
            'active' => true
        ]);

        $user->attachRole($role);

        return redirect('/manager/public/'.$user->id);
    }

    public function adminedit($id)
    {
        $manager = User::find($id);
        $users_count = User::all()->count();
        $roles = Role::all();
        return view('manager.edit', ['manager' => $manager, 'count' => $users_count, 'roles' => $roles]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);

        $role = Role::find($request->role);

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->otdel = $request->otdel;
        $user->phone = $request->phone;
        $user->skype = $request->skype;
        $user->worktime = $request->worktime;
        $user->bornyear = $request->bornyear;
        $user->salary = $request->salary;
        if($request->password != ''){
            $user->password = bcrypt($request->password);
        }
        $user->save();
        $user->detachAllRoles();
        $user->attachRole($role);

        return redirect('/manager/public/'.$user->id);
    }

    public function disabled(Request $request)
    {
        $user = User::find($request->id);
        $user->active = $request->status;
        $user->save();
        return redirect('/managers');
    }

    public function unactive()
    {
        $admin = User::find(3);
       return view('manager.unactive', ['admin' => $admin]);
    }

    public function destroy(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();

        return redirect('/managers');
    }
}
