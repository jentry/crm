<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Coming;
use App\User;
use App\Premia;
use App\Avans;
use App\Daysalary;

class SalaryController extends Controller
{
    public function index(Request $request)
    {
        $me = Auth::user();

        $monthes_array = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        $current_month = date('n');
        $month_name = $monthes_array[$current_month - 1];

        return view('salary.index', ['month' => $month_name, 'id' => $me->id]);
    }

    public function foruser(Request $request)
    {

        $user_id = $request->userId;
        $user = User::find($user_id);
        $res = [];
        foreach($request->days as $dayy){
            $d = $request->year.'-'.$request->month.'-'.$dayy;
            $day = new \DateTime($d);
            $comings = Coming::where([['user_id', $user_id], ['day', $day]])->get();
            $res_time = 0;
            $startJob = '';
            $finishJob = '';
            if(count($comings) == 1){
                $t = date_diff(new \DateTime($comings[0]->outtime), new \DateTime($comings[0]->intime))->h;
                $res_time = $res_time + $t;
            }
            elseif(count($comings) > 1) {
                //$t = date_diff(new \DateTime($comings[0]->outtime), new \DateTime($comings[count($comings) - 1]->intime))->h;
                //$res_time = $res_time + $t;

                foreach($comings as $coming){
                    $t = date_diff(new \DateTime($coming->outtime), new \DateTime($coming->intime))->h;
                    $res_time = $res_time + $t;
                }
            }

            if(count($comings) == 1){
                $startJob = (new \DateTime($comings[0]->intime))->format('H:i');
                $finishJob = (new \DateTime($comings[0]->outtime))->format('H:i');
            }
            elseif(count($comings) > 1){
                $startJob = (new \DateTime($comings[0]->intime))->format('H:i');
                $finishJob = (new \DateTime($comings[count($comings)-1]->outtime))->format('H:i');
            }

            if((int)$dayy < 10){
                $dd = '0'.$dayy;
            }
            else{
                $dd = $dayy;
            }
            if((int)$request->month < 10){
                $mm = '0'.$request->month;
            }
            else{
                $mm = $request->month;
            }

            $premia = Premia::where([['day', $dd], ['month', $mm], ['year', $request->year], ['user_id', $user_id]])->first();
            if(empty($premia)){
                $premia = ['summ' => 0];
            }

            $daySalary = Daysalary::where('date', $day)->first();
            if($daySalary){
                $res_summ = $daySalary->summ;
            }
            else{
                $res_summ = $res_time*(int)$user->salary;
            }

            $r = ['resTime' => $res_time, 'resSumm' => $res_summ, 'startJob' => $startJob, 'finishJob' => $finishJob, 'premia' => $premia];

            $res[$dayy] = $r;
        }
        return $res;
    }

    public function monthsalary(Request $request)
    {
        $daysCount = (int) $request->daysCount;//кол-во рабочих дней в месяце
        $workHoursinMonth = $daysCount*8;
        $allDaysCount = (int) $request->allDaysCount;//кол-во дней в месяце
        $user = User::find($request->userId);
        $premia = 0;//премия
        $oklad = (int)$user->salary*$daysCount*8;//оклад
        $res_time = 0;//отработанных часов в месяц
        foreach($request->days as $dayy) {
            $d = $request->year . '-' . $request->month . '-' . $dayy;
            $day = new \DateTime($d);
            $comings = Coming::where([['user_id', $user->id], ['day', $day]])->get();

            if(count($comings) == 1){
                $t = date_diff(new \DateTime($comings[0]->outtime), new \DateTime($comings[0]->intime))->h;
                $res_time = $res_time + $t;
            }
            elseif(count($comings) > 1) {
                foreach($comings as $coming){
                    $t = date_diff(new \DateTime($coming->outtime), new \DateTime($coming->intime))->h;
                    $res_time = $res_time + $t;
                }
            }

            $month = $request->month;
            if($month < 10){
                $month = '0'.$month;
            }
            $premiaDay = $dayy;
            if($premiaDay < 10){
                $premiaDay = '0'.$premiaDay;
            }
            $premias = Premia::where([
                ['day', $premiaDay],
                ['month', $month],
                ['year', $request->year],
                ['user_id', $user->id]
            ])->get();
            foreach($premias as $prem){
                $premia = $premia + (int) $prem->summ;
            }
        }
        $factSalary = $res_time * (int)$user->salary;
        $shtraf = $workHoursinMonth - $res_time;//штраф
        $month = $request->month;
        if($month < 10){
            $month = '0'.$month;
        }
        $avans = Avans::where([
            ['month', $month],
            ['year', $request->year],
            ['user_id', $user->id]
        ])->first();
        if(isset($avans)){
            $avansSumm = $avans->summ;
        }
        else{
            $avansSumm = 0;
        }

        $result = $oklad - ($shtraf*(int)$user->salary) - $avansSumm + $premia;

        return response()->json([
            'name' => $user->name,
            'oklad' => $oklad,
            'allDaysCount' => $allDaysCount,
            'jobDays' => $daysCount,
            'factSalary' => $factSalary,
            'shtraf' => $shtraf,
            'shtrafSumm' => $shtraf*(int)$user->salary,
            'avans' => $avansSumm,
            'premia' => $premia,
            'result' => $result
        ]);
    }
}

