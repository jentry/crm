<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\User;
use Auth;
use DB;

class MessageController extends Controller
{
    public function index()
    {
        $me = Auth::user()->id;
        $meout = Message::where('author_id', $me)->lists('target_id')->toArray();
        $my = Message::where('target_id', $me)->lists('author_id')->toArray();
        $my = array_merge($my, $meout);
        $my = array_unique($my);
        $dialogs = User::whereIn('id', $my)->get();

        return view('message.index', ['dialogs' => $dialogs, 'me' => $me]);
    }

    public function add($id = null)
    {
        $users = User::all();
        $me = Auth::user()->id;
        $meout = Message::where('author_id', $me)->lists('target_id');
        $my = Message::where('target_id', $me)->lists('author_id')->union($meout)->toArray();
        $my = array_unique($my);
        $dialogs = User::whereIn('id', $my)->get();
        return view('message.add', ['id' => $id, 'users' => $users, 'dialogs' => $dialogs]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'target_id' => 'required',
            'message' => 'required|min:2'
        ]);

        Message::create([
            'author_id' => Auth::user()->id,
            'target_id' => $request->target_id,
            'message' => $request->message,
            'reader' => false,
            'deleted' => false
        ]);

        return redirect('/messages');
    }

    public function stores(Request $request)
    {
        $this->validate($request, [
            'target_id' => 'required',
            'message' => 'required|min:2'
        ]);

        Message::create([
            'author_id' => Auth::user()->id,
            'target_id' => $request->target_id,
            'message' => $request->message,
            'reader' => false,
            'deleted' => false
        ]);

        return redirect('/message/single/'.$request->target_id);
    }

    public function single($id)
    {
        $me = Auth::user()->id;
        $meout = Message::where([['author_id', $me], ['target_id', $id]]);
        $my = Message::where([['author_id', $id], ['target_id', $me]])->union($meout)->get();
        $dialog = $my->sortBy('created_at')->unique('id')->values()->all();
        //$dialog = $dialog;
        $meUser = User::find($me);
        $uUser = User::find($id);

        //all
        $meout1 = Message::where('author_id', $me)->lists('target_id');
        $my1 = Message::where('target_id', $me)->lists('author_id')->union($meout1)->toArray();
        $my1 = array_unique($my1);
        $dialogs1 = User::whereIn('id', $my1)->get();
        //return $dialogs1;
        return view('message.single', ['dialog' => $dialog, 'id' => $id, 'uUser' => $uUser, 'dialogs' => $dialogs1]);
    }

    public function destroy(Request $request)
    {
        $message = Message::find($request->id);
        $message->delete();
    }
}
