<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Bican\Roles\Models\Role;
use App\User;

class RoleController extends Controller
{
    public function index()
    {
        $all = Role::all();
        return view('roles.index', ['all' => $all]);
    }

    public function add()
    {
        return view('roles.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required',
            'slug' => 'required'
        ]);

        Role::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description
        ]);

        return redirect('/managersrole');
    }

    public function show($id)
    {
        $role = Role::find($id);
        return view('roles.show', ['role' => $role]);
    }

    public function destroy(Request $request)
    {
        $role = Role::find($request->id);
        $role->delete();
        return redirect('/managersrole');
    }
}
