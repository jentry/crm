<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use App\Premia;

class PremiaController extends Controller
{

    public function index()
    {
        $users = User::all();
        $users_count = $users->count();
        return view('premia.index', ['users' => $users, 'count' => $users_count]);
    }

    public function add($id)
    {
        $user = User::find($id);
        $users = User::all();
        $users_count = $users->count();
        return view('premia.add', ['user' => $user, 'count' => $users_count]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required',
            'summ' => 'required'
        ]);
        $datas = explode(' ', $request->data);
        $day = $datas[0];
        if($day < 10){
            $day = '0'.$day;
        }

        Premia::create([
            'user_id' => $request->user,
            'day' => $day,
            'month' => $datas[1],
            'year' => $datas[2],
            'summ' => $request->summ
        ]);

        return redirect('/premia/add/'.$request->user)->with('message', '������ ���������');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'summ' => 'required'
        ]);

        $premia = Premia::find($request->id);
        $premia->summ = $request->summ;
        $premia->save();
        return redirect('/premia/add/'.$request->user)->with('message', '������ ���������');
    }
}
