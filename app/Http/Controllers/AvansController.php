<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Avans;

class AvansController extends Controller
{
    public function index()
    {
        $users = User::all();
        $users_count = $users->count();
        return view('avans.index', ['count' => $users_count, 'users' => $users]);
    }

    public function add($id)
    {
        $user = User::find($id);
        $users = User::all();
        $users_count = $users->count();
        return view('avans.add', ['user' => $user, 'count' => $users_count]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required',
            'summ' => 'required'
        ]);
        $datas = explode(' ', $request->data);

        Avans::create([
            'user_id' => $request->user,
            'month' => $datas[0],
            'year' => $datas[1],
            'summ' => $request->summ
        ]);

        return redirect('/avans/add/'.$request->user)->with('message', '����� ��������');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'summ' => 'required'
        ]);

        $avans = Avans::find($request->id);
        $avans->summ = $request->summ;
        $avans->save();
        return redirect('/avans/add/'.$request->user)->with('message', '����� ���������');
    }
}
