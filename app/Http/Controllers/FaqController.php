<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Faq;

class FaqController extends Controller
{
    public function index()
    {
        $all = Faq::all();
        return view('faq.index', ['all' => $all]);
    }

    public function create()
    {
        return view('faq.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required'
        ]);

        $audioname = '';
        if ($request->hasFile('audio')) {
            //$destinationPath = public_path().'/upload/audio/'; // upload path
            $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/audio/";
            $extension = $request->file('audio')->getClientOriginalExtension(); // getting image extension
            $audioname = rand(11111,99999).'.'.$extension; // renameing image
            $request->file('audio')->move($destinationPath, $audioname);
        }

        Faq::create([
            'title' => $request->title,
            'text' => $request->text,
            'audio' => $audioname
        ]);

        return redirect('/faq/')->with('mess', 'Совет добавлен');
    }

    public function show($id)
    {
        $faq = Faq::find($id);
        return view('faq.show', ['faq' => $faq]);
    }

    public function edit($id)
    {
        $faq = Faq::find($id);
        return view('faq.update', ['faq' => $faq]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required'
        ]);

        $audioname = '';
        if ($request->hasFile('audio')) {
            //$destinationPath = public_path().'/upload/audio/'; // upload path
            $destinationPath = "/home/t/tigranla/db.es-stone.ru/public_html/upload/audio/";
            $extension = $request->file('audio')->getClientOriginalExtension(); // getting image extension
            $audioname = rand(11111,99999).'.'.$extension; // renameing image
            $request->file('audio')->move($destinationPath, $audioname);
        }

        $faq = Faq::find($request->id);
        $faq->title = $request->title;
        $faq->text = $request->text;
        if ($request->hasFile('audio')) {
            $faq->audio = $audioname;
        }
        $faq->save();


        return redirect('/faq/')->with('mess', 'Совет отредактирован');
    }

    public function destroy(Request $request)
    {
        $faq = Faq::find($request->id);
        $faq->delete();
        return redirect('/faq/')->with('mess', 'Совет удален');
    }
}
