<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

//Route::get('/home', 'HomeController@index');

Route::get('/', function(){
    return redirect('/newclient');
});
Route::get('/user/unactive', 'ManagerController@unactive');

Route::group(['middleware' => ['auth', 'active']], function () {

    Route::get('/managers', 'ManagerController@index');
    Route::get('/manager/profile', 'ManagerController@show');
    Route::put('/manager/edit', 'ManagerController@edit');
    Route::get('/manager/public/{id}', 'ManagerController@manager');
    Route::get('/helping', 'ManagerController@helping');
    Route::get('manager/add', 'ManagerController@add');
    Route::post('manager/add', 'ManagerController@store');
    Route::get('manager/adminedit/{id}', 'ManagerController@adminedit');
    Route::put('manager/adminedit', 'ManagerController@update');
    Route::put('manager/disable', 'ManagerController@disabled');
    Route::delete('/manager/destroy', 'ManagerController@destroy');

    Route::get('/managersrole', 'RoleController@index');
    Route::get('/managersrole/add', 'RoleController@add');
    Route::post('/managersrole/add', 'RoleController@store');
    Route::get('/managersrole/{id}', 'RoleController@show');
    Route::delete('/managersrole/delete', 'RoleController@destroy');

    Route::get('/newclient', 'ClientController@index');
    Route::post('client/search', 'ClientController@search');
    Route::post('/client/randomsite', 'ClientController@randomsite');
    Route::post('/client/add', 'ClientController@add');
    Route::post('/client/sort', 'ClientController@sort');
    Route::post('/client/edit', 'ClientController@edit');
    Route::put('/client/edit', 'ClientController@update');
    Route::post('/client/edit/deadline', 'ClientController@edittime');
    Route::put('/client/update/deadline', 'ClientController@updatetime');
    Route::post('/client/comments', 'ClientController@comments');
    Route::post('/client/comments/add', 'ClientController@commentadd');
    Route::post('/client/edit/status', 'ClientController@editstatus');
    Route::put('/client/update/status', 'ClientController@updatestatus');
    Route::post('/client/file', 'ClientController@files');

    Route::get('faq', 'FaqController@index');
    Route::get('faq/create', 'FaqController@create');
    Route::post('faq/store', 'FaqController@store');
    Route::get('faq/{id}', 'FaqController@show');
    Route::get('faq/edit/{id}', 'FaqController@edit');
    Route::put('faq/update', 'FaqController@update');
    Route::delete('faq/delete', 'FaqController@destroy');


    Route::get('tasks', 'TaskController@index');
    Route::get('/task/add/{userid?}', 'TaskController@add');
    Route::post('task/create', 'TaskController@create');
    Route::get('/task/{id}', 'TaskController@show');
    Route::put('/task/changefav', 'TaskController@changefav');
    Route::post('/task/showsub', 'TaskController@sub');
    Route::get('/task/edit/{id}', 'TaskController@edit');
    Route::put('/task/update', 'TaskController@update');
    Route::post('/task/percent', 'TaskController@percent');
    Route::put('/task/percent', 'TaskController@percentadd');
    Route::post('/task/status', 'TaskController@status');
    Route::put('/task/status', 'TaskController@statusadd');
    Route::post('/task/finish', 'TaskController@finish');
    Route::post('/tasks/sort/params', 'TaskController@sort');

    Route::post('/taskcomment/add', 'TaskController@addcomment');
    Route::post('/taskcomment/get', 'TaskController@getcomment');
    Route::put('/taskcomment/edit', 'TaskController@editcomment');

    Route::post('/susers/get', 'TaskController@susersget');
    Route::put('/susers/update', 'TaskController@suserupdate');

    Route::post('/auditors/get', 'TaskController@auditorsget');
    Route::put('/auditors/update', 'TaskController@auditorsupdate');

    Route::get('/messages', 'MessageController@index');
    Route::get('/message/add/{id?}', 'MessageController@add');
    Route::post('/message/add', 'MessageController@store');
    Route::post('/messages/add', 'MessageController@stores');
    Route::get('/message/single/{id}', 'MessageController@single');
    Route::delete('/message/delete', 'MessageController@destroy');

    Route::get('/salary', 'SalaryController@index');
    Route::post('/salary/foruser', 'SalaryController@foruser');
    Route::post('/salary/monthsalary', 'SalaryController@monthsalary');

    Route::get('/premia', 'PremiaController@index');
    Route::get('/premia/add/{id}', 'PremiaController@add');
    Route::post('/premia/store', 'PremiaController@store');
    Route::put('/premia/update', 'PremiaController@update');

    Route::get('/avans', 'AvansController@index');
    Route::get('/avans/add/{id}', 'AvansController@add');
    Route::post('/avans/store', 'AvansController@store');
    Route::put('/avans/update', 'AvansController@update');

    Route::post('/notifications/reads', 'NotificationController@index');
    Route::get('/search', 'SearchController@index');

    Route::get('/daysalary', 'DaysalaryController@index');
    Route::get('/daysalary/add/{id}', 'DaysalaryController@add');
    Route::post('/daysalary/store', 'DaysalaryController@store');
    Route::put('/daysalary/update', 'DaysalaryController@update');
});
