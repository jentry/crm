<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avans extends Model
{
    protected $table = 'avanses';

    protected $fillable = ['user_id', 'month', 'summ', 'year'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
