<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taskcomment extends Model
{
    protected $fillable = ['text', 'user_id', 'task_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function task()
    {
        return $this->belongsTo('App\Task', 'task_id');
    }
}
