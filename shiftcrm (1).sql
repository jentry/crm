-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 24 2016 г., 12:55
-- Версия сервера: 5.5.45
-- Версия PHP: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `shiftcrm`
--

-- --------------------------------------------------------

--
-- Структура таблицы `avanses`
--

CREATE TABLE IF NOT EXISTS `avanses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summ` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `avanses`
--

INSERT INTO `avanses` (`id`, `user_id`, `month`, `year`, `summ`, `created_at`, `updated_at`) VALUES
(1, 2, '06', '2016', 2800, '2016-06-10 09:36:42', '2016-06-10 10:20:35');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `islike` tinyint(1) NOT NULL,
  `blacklist` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `name`, `deadline`, `comment`, `islike`, `blacklist`, `created_at`, `updated_at`, `status`, `user_id`) VALUES
(3, 'habrahabr.ru', '', 'тест', 0, 0, '2016-05-12 08:47:18', '2016-05-12 08:47:18', '', 2),
(4, 'habrahabr.ru', '', 'тест 2', 0, 0, '2016-05-12 08:48:43', '2016-05-12 08:48:43', '', 2),
(5, 'habrahabr.ru', '24 06 2016', '        тест    ', 1, 0, '2016-05-12 08:50:26', '2016-06-15 13:06:35', 'Созвон', 2),
(6, 'habrahabr.ru', '28 05 2016', 'тест 3', 1, 0, '2016-05-12 08:50:41', '2016-06-15 13:03:46', 'В проект', 2),
(7, 'habrahabr.ru', '27 05 2016', 'тест 4', 1, 0, '2016-05-12 09:05:31', '2016-05-25 08:03:31', 'Созвон', 2),
(8, 'megashara.com', '', 'sdasd', 0, 0, '2016-05-12 09:28:47', '2016-05-12 09:28:47', '', 2),
(9, 'lostfilm.tv', '', 'в черный список', 0, 1, '2016-05-12 09:31:04', '2016-05-12 09:31:04', '', 2),
(10, 'habrahabr.ru', '30 05 2016', 'sadasd', 1, 0, '2016-05-12 09:32:07', '2016-05-12 09:32:07', 'Встреча', 2),
(13, 'vk.com', '31 05 2016', 'яндекс', 1, 0, '2016-05-13 16:38:22', '2016-05-13 16:38:22', 'Созвон', 3),
(14, 'vk.com', '19 05 2016', 'тест', 1, 0, '2016-05-16 09:01:22', '2016-05-16 09:01:22', 'Созвон', 2),
(15, 'vk.com', '19 05 2016', 'тест', 1, 0, '2016-05-16 09:01:29', '2016-05-16 09:01:29', 'Созвон', 2),
(16, 'deloz.h1.ru', '19 05 2016', 'dfsdf', 1, 0, '2016-05-16 09:02:50', '2016-05-16 09:02:50', 'Созвон', 2),
(17, 'deloz.h1.ru', '19 05 2016', 'dfsdf', 1, 0, '2016-05-16 09:03:18', '2016-05-16 09:03:18', 'КП', 2),
(18, 'deloz.h1.ru', '19 05 2016', 'dfsdf', 1, 0, '2016-05-16 09:03:24', '2016-05-16 09:03:24', 'КП', 2),
(19, 'deloz.h1.ru', '19 05 2016', 'sdfsdfsdfsdfsdfsdfsdf', 1, 0, '2016-05-16 09:03:30', '2016-05-16 09:03:30', 'Встреча', 2),
(20, 'vk.com', '17 05 2016', 'xcxc', 1, 0, '2016-05-16 09:04:13', '2016-05-16 09:04:13', 'Созвон', 2),
(21, 'deloz.h1.ru', '', '', 1, 0, '2016-05-16 09:04:59', '2016-05-16 09:04:59', 'Созвон', 2),
(22, 'prigotovit.org', '', 'sdfdsf', 0, 0, '2016-05-16 09:05:53', '2016-05-16 09:05:53', '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `comings`
--

CREATE TABLE IF NOT EXISTS `comings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `intime` time NOT NULL,
  `outtime` time NOT NULL,
  `day` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Дамп данных таблицы `comings`
--

INSERT INTO `comings` (`id`, `user_id`, `intime`, `outtime`, `day`, `created_at`, `updated_at`) VALUES
(2, 2, '09:42:00', '17:43:00', '2016-06-01 00:00:00', '2016-06-02 06:42:06', '2016-06-02 06:43:10'),
(3, 2, '09:43:25', '09:43:25', '2016-06-02 00:00:00', '2016-06-02 06:43:25', '2016-06-02 06:43:25'),
(4, 3, '09:47:44', '11:47:44', '2016-06-02 00:00:00', '2016-06-02 06:47:44', '2016-06-02 06:47:44'),
(5, 2, '12:12:00', '12:12:00', '2016-06-02 00:00:00', '2016-06-02 09:12:26', '2016-06-02 09:12:26'),
(6, 2, '13:54:00', '14:38:00', '2016-06-02 00:00:00', '2016-06-02 10:54:10', '2016-06-02 11:38:32'),
(7, 2, '14:38:00', '15:56:00', '2016-06-02 00:00:00', '2016-06-02 11:38:38', '2016-06-02 12:56:53'),
(8, 3, '14:39:00', '14:39:00', '2016-06-02 00:00:00', '2016-06-02 11:39:24', '2016-06-02 11:39:24'),
(9, 3, '15:44:00', '15:44:00', '2016-06-02 00:00:00', '2016-06-02 12:44:52', '2016-06-02 12:44:52'),
(10, 2, '15:58:00', '15:58:00', '2016-06-02 00:00:00', '2016-06-02 12:58:10', '2016-06-02 12:58:52'),
(11, 2, '15:59:00', '16:59:00', '2016-06-02 00:00:00', '2016-06-02 12:59:35', '2016-06-02 12:59:35'),
(12, 3, '16:19:00', '17:19:00', '2016-06-02 00:00:00', '2016-06-02 13:19:31', '2016-06-02 13:19:31'),
(13, 3, '07:54:00', '08:54:00', '2016-06-03 00:00:00', '2016-06-03 04:54:18', '2016-06-03 04:54:18'),
(14, 2, '07:59:00', '08:59:00', '2016-06-03 00:00:00', '2016-06-03 04:59:46', '2016-06-03 04:59:46'),
(15, 3, '09:04:00', '10:04:00', '2016-06-03 00:00:00', '2016-06-03 06:04:58', '2016-06-03 06:04:58'),
(16, 2, '09:22:00', '10:22:00', '2016-06-03 00:00:00', '2016-06-03 06:22:08', '2016-06-03 06:22:08'),
(17, 2, '09:14:00', '10:14:00', '2016-06-10 00:00:00', '2016-06-10 06:14:59', '2016-06-10 06:14:59'),
(18, 3, '10:12:00', '11:12:00', '2016-06-10 00:00:00', '2016-06-10 07:12:30', '2016-06-10 07:12:30'),
(19, 3, '12:35:00', '13:35:00', '2016-06-10 00:00:00', '2016-06-10 09:35:05', '2016-06-10 09:35:05'),
(20, 2, '12:37:00', '13:37:00', '2016-06-10 00:00:00', '2016-06-10 09:37:17', '2016-06-10 09:37:17'),
(21, 3, '13:17:00', '14:17:00', '2016-06-10 00:00:00', '2016-06-10 10:17:08', '2016-06-10 10:17:08'),
(22, 3, '15:10:00', '16:14:00', '2016-06-10 00:00:00', '2016-06-10 12:10:38', '2016-06-10 13:14:46'),
(23, 2, '16:16:00', '17:16:00', '2016-06-10 00:00:00', '2016-06-10 13:16:24', '2016-06-10 13:16:24'),
(24, 2, '09:44:00', '10:44:00', '2016-06-12 00:00:00', '2016-06-12 06:44:46', '2016-06-12 06:44:46'),
(25, 5, '10:18:00', '11:18:00', '2016-06-12 00:00:00', '2016-06-12 07:18:01', '2016-06-12 07:18:01'),
(26, 3, '16:02:00', '17:02:00', '2016-06-15 00:00:00', '2016-06-15 13:02:23', '2016-06-15 13:02:23'),
(27, 3, '22:56:00', '23:56:00', '2016-06-15 00:00:00', '2016-06-15 19:56:49', '2016-06-15 19:56:49'),
(28, 3, '11:17:00', '12:17:00', '2016-06-16 00:00:00', '2016-06-16 08:17:28', '2016-06-16 08:17:28'),
(29, 2, '11:50:00', '12:50:00', '2016-06-16 00:00:00', '2016-06-16 08:50:52', '2016-06-16 08:50:52'),
(30, 3, '13:35:00', '14:35:00', '2016-06-16 00:00:00', '2016-06-16 10:35:06', '2016-06-16 10:35:06'),
(31, 2, '13:35:00', '14:35:00', '2016-06-16 00:00:00', '2016-06-16 10:35:38', '2016-06-16 10:35:38'),
(32, 3, '17:20:00', '18:20:00', '2016-06-21 00:00:00', '2016-06-21 14:20:17', '2016-06-21 14:20:17'),
(33, 2, '17:20:00', '18:20:00', '2016-06-21 00:00:00', '2016-06-21 14:20:29', '2016-06-21 14:20:29'),
(34, 2, '19:25:00', '20:25:00', '2016-06-21 00:00:00', '2016-06-21 16:25:42', '2016-06-21 16:25:42'),
(35, 2, '20:22:00', '21:22:00', '2016-06-21 00:00:00', '2016-06-21 17:22:19', '2016-06-21 17:22:19'),
(36, 2, '20:24:00', '21:24:00', '2016-06-21 00:00:00', '2016-06-21 17:24:25', '2016-06-21 17:24:25'),
(37, 3, '22:15:00', '23:15:00', '2016-06-23 00:00:00', '2016-06-23 19:15:06', '2016-06-23 19:15:06'),
(38, 3, '12:24:00', '13:24:00', '2016-06-24 00:00:00', '2016-06-24 09:24:32', '2016-06-24 09:24:32');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `text`, `client_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'новый', 6, 3, '2016-05-13 11:09:47', '2016-05-13 11:09:47'),
(2, 'привет', 5, 3, '2016-05-13 11:19:59', '2016-05-13 11:19:59'),
(3, 'тест', 5, 2, '2016-05-23 05:48:31', '2016-05-23 05:48:31'),
(4, 'проверка', 5, 3, '2016-06-15 13:10:15', '2016-06-15 13:10:15'),
(5, 'scsdf', 5, 3, '2016-06-15 16:15:59', '2016-06-15 16:15:59');

-- --------------------------------------------------------

--
-- Структура таблицы `daysalaries`
--

CREATE TABLE IF NOT EXISTS `daysalaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summ` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `daysalaries`
--

INSERT INTO `daysalaries` (`id`, `user_id`, `summ`, `date`, `created_at`, `updated_at`) VALUES
(4, 2, 1500, '2016-06-21 00:00:00', '2016-06-21 17:23:45', '2016-06-21 17:23:45');

-- --------------------------------------------------------

--
-- Структура таблицы `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `audio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `text`, `audio`, `created_at`, `updated_at`) VALUES
(2, 'Как начать разговор?', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Клиент</td>\r\n			<td>Алло</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Оператор:</td>\r\n			<td>Добрый день, беспокоит studio-shift с кем я могу пообщать по поводу Вашего сайта. С Вами?</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Клиент</td>\r\n			<td>Наверное, с мной.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Оператор:</td>\r\n			<td>Как я могу к Вам обратиться?Как Вас зовут?</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Клиент</td>\r\n			<td>Владимир</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Оператор:</td>\r\n			<td>Ваш сайт zavod-vvkz.ru на данный момент занимает по коммерческим запросам ужасные места и я хотела предложить услуги по продвижению.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '27886.mp3', '2016-05-12 06:36:30', '2016-05-12 06:36:30'),
(3, 'Как продолжить разговор', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Клиент</td>\r\n			<td>Алло</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Оператор:</td>\r\n			<td>Добрый день, беспокоит studio-shift с кем я могу пообщать по поводу Вашего сайта. С Вами?</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Клиент</td>\r\n			<td>Наверное, с мной.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Оператор:</td>\r\n			<td>Как я могу к Вам обратиться?Как Вас зовут?</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Клиент</td>\r\n			<td>Владимир</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Оператор:</td>\r\n			<td>Ваш сайт zavod-vvkz.ru на данный момент занимает по коммерческим запросам ужасные места и я хотела предложить услуги по продвижению.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '83352.mp3', '2016-05-12 08:58:35', '2016-05-12 08:58:35');

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `readed` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `author_id`, `target_id`, `message`, `readed`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 2, 5, 'Привет', 0, 0, '2016-05-17 04:59:00', '2016-05-17 04:59:00'),
(3, 5, 2, 'Привет', 0, 0, '2016-05-17 05:35:47', '2016-05-17 05:35:47'),
(4, 5, 2, 'привет', 0, 0, '2016-05-17 07:56:05', '2016-05-17 07:56:05'),
(7, 2, 3, 'привет', 0, 0, '2016-06-21 14:45:07', '2016-06-21 14:45:07');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_04_28_145142_ChangeUserTable', 2),
('2015_01_15_105324_create_roles_table', 3),
('2015_01_15_114412_create_role_user_table', 3),
('2015_01_26_115212_create_permissions_table', 3),
('2015_01_26_115523_create_permission_role_table', 3),
('2015_02_09_132439_create_permission_user_table', 3),
('2016_05_11_090217_create_client_table', 4),
('2016_05_11_104839_create_faqs_table', 5),
('2016_05_12_113845_change_clients_table', 6),
('2016_05_13_063841_create_task_table', 7),
('2016_05_13_072749_create_task_files_table', 7),
('2016_05_13_073229_create_auditors_table', 7),
('2016_05_13_073614_change_tasks_table', 8),
('2016_05_13_074705_add_end_to_tasks', 9),
('2016_05_13_131815_create_comments_table', 10),
('2016_05_13_135332_change_client_t', 11),
('2016_05_14_111325_create_taskcomments_table', 12),
('2016_05_14_152315_create_task_susers_table', 12),
('2016_05_16_073939_create_taskcomments_files_table', 13),
('2016_05_16_171901_create_messages_table', 14),
('2016_06_01_123647_create_times_table', 15),
('2016_06_01_123851_create_sessions_table', 15),
('2016_06_01_124152_create_times_tables', 16),
('2016_06_01_135901_create_comings_table', 17),
('2016_06_02_092728_change_comings_table', 18),
('2016_06_02_161400_create_premia_table', 19),
('2016_06_10_101341_create_avans_table', 20),
('2016_06_12_094804_create_notification_table', 21),
('2016_06_21_185338_create_daysalary_table', 22),
('2016_06_24_123743_create_options_table', 23);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `readed` tinyint(1) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `message`, `readed`, `type`, `created_at`, `updated_at`) VALUES
(2, 5, 'К задаче <a href="/task/2">Первая тестовая задача</a> добавлен комментарий', 1, 1, '2016-06-12 07:15:40', '2016-06-12 07:15:40'),
(3, 3, 'К задаче <a href="/task/2">Первая тестовая задача</a> добавлен комментарий', 0, 1, '2016-06-12 07:15:40', '2016-06-12 07:15:40'),
(4, 2, 'К задаче <a href="/task/2">Первая тестовая задача</a> добавлен комментарий', 1, 1, '2016-06-12 07:15:40', '2016-06-12 07:15:40'),
(5, 2, 'Вас назначено исполнителем на задачу <a href="/task/16">Сдача crm</a>', 1, 2, '2016-06-15 16:18:38', '2016-06-15 16:18:38'),
(6, 2, 'Вас назначено исполнителем на задачу <a href="/task/17">Сдача crm</a>', 1, 2, '2016-06-15 16:23:19', '2016-06-15 16:23:19'),
(7, 5, 'Задача <a href="/task/2">Первая тестовая задача</a> закрыта', 0, 1, '2016-06-21 14:38:03', '2016-06-21 14:38:03'),
(8, 2, 'Задача <a href="/task/2">Первая тестовая задача</a> закрыта', 0, 1, '2016-06-21 14:38:03', '2016-06-21 14:38:03'),
(9, 3, 'Задача <a href="/task/2">Первая тестовая задача</a> закрыта', 0, 1, '2016-06-21 14:38:03', '2016-06-21 14:38:03'),
(10, 5, 'Задача <a href="/task/2">Первая тестовая задача</a> закрыта', 0, 1, '2016-06-21 16:05:57', '2016-06-21 16:05:57'),
(11, 2, 'Задача <a href="/task/2">Первая тестовая задача</a> закрыта', 0, 1, '2016-06-21 16:05:57', '2016-06-21 16:05:57'),
(12, 3, 'Задача <a href="/task/2">Первая тестовая задача</a> закрыта', 0, 1, '2016-06-21 16:05:57', '2016-06-21 16:05:57');

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `file`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '83867.txt', 3, '2016-06-23 21:00:00', '2016-06-23 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `permission_user`
--

CREATE TABLE IF NOT EXISTS `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `premias`
--

CREATE TABLE IF NOT EXISTS `premias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summ` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `premias`
--

INSERT INTO `premias` (`id`, `user_id`, `day`, `month`, `year`, `summ`, `created_at`, `updated_at`) VALUES
(1, 2, '01', '06', '2016', 1700, '2016-06-03 06:05:05', '2016-06-03 06:46:13');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin role', 1, '2016-05-10 06:26:38', '2016-05-10 06:26:38'),
(2, 'Developer', 'developer', 'developer role', 1, '2016-05-10 06:27:28', '2016-05-10 06:27:28'),
(3, 'Callcenter', 'callcenter', 'Callcenter role', 1, '2016-05-10 06:28:06', '2016-05-10 06:28:06');

-- --------------------------------------------------------

--
-- Структура таблицы `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2016-05-10 06:26:38', '2016-05-10 06:26:38'),
(4, 2, 2, NULL, NULL),
(5, 3, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('023dd8b576ec31906bb1b8b564cd4d483e16d39d', 3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36 OPR/38.0.2220.31', 'YTo3OntzOjY6Il90b2tlbiI7czo0MDoiMmFtNFRQWGhGYjZPZVh2VXdMZDgzbU5RTDNwU2kyU3VUMWJlRG5ISSI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjMwOiJodHRwOi8vc2hpZnRjcm06ODA3OC9uZXdjbGllbnQiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjM7czo2OiJjb21pbmciO2k6Mzg7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0NjY3NjIwMTA7czoxOiJjIjtpOjE0NjY3NjAyNDE7czoxOiJsIjtzOjE6IjAiO319', 1466762010),
('465997f80de5d3b61880257859fc0be50406720e', 3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36 OPR/38.0.2220.31', 'YTo3OntzOjY6Il90b2tlbiI7czo0MDoiVkU3SlQ4QUFreHU0NkdONm1qZ3FrblZlV2VZaW9rZmxZNGV3NUtLTCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzA6Imh0dHA6Ly9zaGlmdGNybTo4MDc4L25ld2NsaWVudCI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czozOiJ1cmwiO2E6MDp7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjM7czo2OiJjb21pbmciO2k6Mzc7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0NjY3MTE3NzI7czoxOiJjIjtpOjE0NjY3MDkyOTI7czoxOiJsIjtzOjE6IjAiO319', 1466711772);

-- --------------------------------------------------------

--
-- Структура таблицы `taskcomments`
--

CREATE TABLE IF NOT EXISTS `taskcomments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `taskcomments`
--

INSERT INTO `taskcomments` (`id`, `text`, `task_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Первый комментарий', 2, 2, '2016-05-16 04:45:59', '2016-05-16 04:45:59'),
(2, 'Второй комментарий', 2, 2, '2016-05-16 04:47:46', '2016-05-16 05:09:55'),
(3, 'тест', 2, 2, '2016-06-12 07:13:07', '2016-06-12 07:13:07'),
(4, 'тест', 2, 2, '2016-06-12 07:14:50', '2016-06-12 07:14:50'),
(5, 'тест', 2, 2, '2016-06-12 07:15:22', '2016-06-12 07:15:22'),
(6, 'тест', 2, 2, '2016-06-12 07:15:40', '2016-06-12 07:15:40');

-- --------------------------------------------------------

--
-- Структура таблицы `taskcomments_files`
--

CREATE TABLE IF NOT EXISTS `taskcomments_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deadline` datetime NOT NULL,
  `fire` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `favorite` tinyint(1) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `description`, `deadline`, `fire`, `user_id`, `author_id`, `favorite`, `status`, `percent`, `created_at`, `updated_at`, `parent_id`, `finished`) VALUES
(2, 'Первая тестовая задача', '<p>Первая тестовая задача 222</p>\r\n', '1970-01-23 15:28:06', 1, 2, 3, 1, 'Принята', 10, '2016-05-14 12:36:23', '2016-06-21 16:05:57', 0, 0),
(4, 'Вторая задача', '<pre>\r\nВторая задача</pre>\r\n', '2016-05-24 08:17:56', 1, 3, 3, 0, 'Назначена', 0, '2016-05-15 05:17:56', '2016-05-15 11:49:56', 2, 0),
(5, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 09:59:33', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 06:59:33', '2016-05-26 07:05:46', 0, 1),
(6, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 11:13:08', 1, 5, 2, 1, 'Назначена', 0, '2016-05-26 07:00:19', '2016-05-26 08:13:08', 0, 0),
(7, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:00:41', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:00:41', '2016-05-26 07:00:41', 0, 0),
(8, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:00:55', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:00:55', '2016-05-26 07:00:55', 0, 0),
(9, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:01:16', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:01:16', '2016-05-26 07:01:16', 0, 0),
(10, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:01:34', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:01:34', '2016-05-26 07:01:34', 0, 0),
(11, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:01:49', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:01:49', '2016-05-26 07:01:49', 0, 0),
(12, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:01:57', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:01:57', '2016-05-26 07:01:57', 0, 0),
(13, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:02:17', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:02:17', '2016-05-26 07:02:17', 0, 0),
(14, 'Сделать аудит', '<p>Сделать аудит</p>\r\n', '2016-05-19 10:02:55', 1, 5, 2, 0, 'Назначена', 0, '2016-05-26 07:02:55', '2016-05-26 07:02:55', 0, 0),
(15, 'Тест', '<p>тест</p>\r\n', '2016-06-24 16:12:53', 1, 5, 2, 0, 'Назначена', 0, '2016-06-02 13:12:54', '2016-06-02 13:12:54', 2, 0),
(16, 'Сдача crm', '<p>Сдача crm</p>\r\n', '2016-06-16 19:18:38', 0, 2, 3, 0, 'Назначена', 0, '2016-06-15 16:18:38', '2016-06-15 16:18:38', 0, 0),
(17, 'Сдача crm', '<p>Сдача crm</p>\r\n', '2016-06-16 19:23:19', 0, 2, 3, 0, 'Назначена', 0, '2016-06-15 16:23:19', '2016-06-15 16:23:19', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_auditors`
--

CREATE TABLE IF NOT EXISTS `tasks_auditors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `tasks_auditors`
--

INSERT INTO `tasks_auditors` (`id`, `task_id`, `user_id`, `created_at`, `updated_at`) VALUES
(15, 2, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_files`
--

CREATE TABLE IF NOT EXISTS `tasks_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tasks_files`
--

INSERT INTO `tasks_files` (`id`, `task_id`, `file_name`, `created_at`, `updated_at`) VALUES
(1, 4, '41696.json', NULL, NULL),
(2, 4, '63129.js', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `task_susers`
--

CREATE TABLE IF NOT EXISTS `task_susers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `task_susers`
--

INSERT INTO `task_susers` (`id`, `task_id`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 4, 2, NULL, NULL),
(17, 2, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `otdel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `worktime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bornyear` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `salary` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `otdel`, `phone`, `skype`, `worktime`, `bornyear`, `foto`, `active`, `salary`) VALUES
(2, 'Евгений Тришин', 'trishin.jenya@yandex.ru', '$2y$10$DksR5NMqTWKi0zHVlUJGKeO1UeEzOgaaX9qVhMcZrIOTJ8DRx8JdG', 'm2Qo86vUcONSIxJHKbEbZlzKzmWngVjR6De3mSe6A8ibSn29TX6ZqIIKqwpT', '2016-04-28 12:38:58', '2016-06-16 10:35:10', 'разработчик фриланс', '+380938160704', 'web_boom', '10:00-18:00', '1990', '70130.png', 1, 200),
(3, 'Тигран', 'tigran@shitf.ru', '$2y$10$YTZ5lFWWsraX9JaMCC0G1.kBblIiwJD1NX.y3178KKvDmjK/lQZwG', 'ZUqJHeBAcflzPsX7DSkLBZKOLg3ycOrrm6BSQGnF1njzMGcEwekhUIcVeGlm', '2016-05-10 06:24:47', '2016-06-10 13:14:46', 'Team lead', '+380936450356', 'diavolos7', '10:00-18:00', '1985', '34638.png', 1, 300),
(5, 'Кристина', 'kristina_burian@yahoo.com', '$2y$10$tmXcs966waJCksjCxAL22OG2o2pvpQ9cKpnowKlDtFs1pp3DfkvMm', '6Z9CblFxAhxRYyQElYKlB9AYWEbAcn72PelxyeY0Bw7smt7ux0K7FqR4JD7d', '2016-05-12 10:25:13', '2016-05-17 07:27:18', 'Дизайнер', '+380938160704', '+380938160704', '10:00-18:00', '1990', '', 1, 400);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
